package plugins.tprovoost.sequenceblocks.infos;

import icy.image.colormap.IcyColorMap;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.sequence.Sequence;
import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarArray;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

public class SetColormaps extends Plugin implements SequenceBlock, PluginBundled
{
    final protected VarSequence sequenceIn = new VarSequence("Input", null);
    final protected VarArray<IcyColorMap> colormaps = new VarArray<IcyColorMap>("Color maps", IcyColorMap[].class,
            new IcyColorMap[0]);

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("input", sequenceIn);
        inputMap.add("colormaps", colormaps);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        // no output here
    }

    @Override
    public void run()
    {
        final Sequence seq = sequenceIn.getValue();
        if (seq == null)
            throw new VarException(sequenceIn, "Sequence is null");

        final IcyColorMap[] cms = colormaps.getValue();
        if (cms == null)
            throw new VarException(colormaps, "Color maps is null");

        for (int c = 0; c < Math.min(seq.getSizeC(), cms.length); c++)
            seq.setColormap(c, cms[c], cms[c].isAlpha());
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
