package plugins.tprovoost.sequenceblocks.remove;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.sequence.Sequence;
import icy.sequence.SequenceUtil;
import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarInteger;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * Block to remove a channel from a Sequence
 * 
 * @author Stephane
 */
public class RemoveChannel extends Plugin implements SequenceBlock, PluginLibrary, PluginBundled
{
    final protected VarSequence inputSequence = new VarSequence("Sequence", null);
    final protected VarInteger channelIdx = new VarInteger("Channel", 0);

    @Override
    public void run()
    {
        Sequence s = inputSequence.getValue();
        if (s == null)
            throw new VarException(inputSequence, "Input sequence is null.");
        if (s.getSizeC() == 1)
            throw new VarException(inputSequence, "Cannot remove channel on single channel image");

        int channel = channelIdx.getValue().intValue();
        if (channel < 0 || channel >= s.getSizeC())
            throw new VarException(channelIdx, "Channel index must be between 0 and " + (s.getSizeC() - 1));

        try
        {
            SequenceUtil.removeChannel(s, channel);
        }
        catch (InterruptedException e)
        {
            // nothing to do
        }
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("sequence", inputSequence);
        inputMap.add("channel", channelIdx);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        //
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
