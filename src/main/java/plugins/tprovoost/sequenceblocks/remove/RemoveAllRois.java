package plugins.tprovoost.sequenceblocks.remove;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.roi.ROI;
import icy.sequence.Sequence;
import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * Block to remove all the {@link ROI} from a Sequence
 * 
 * @author Stephane
 */
public class RemoveAllRois extends Plugin implements SequenceBlock, PluginLibrary, PluginBundled
{
    final protected VarSequence inputSequence = new VarSequence("Sequence", null);

    @Override
    public void run()
    {
        final Sequence s = inputSequence.getValue();
        if (s == null)
            throw new VarException(inputSequence, "Input sequence is null.");

        s.removeAllROI(false);
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("sequence", inputSequence);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        //
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
