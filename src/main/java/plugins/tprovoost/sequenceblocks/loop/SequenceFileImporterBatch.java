package plugins.tprovoost.sequenceblocks.loop;

import java.util.List;

import icy.file.SequenceFileImporter;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import plugins.adufour.blocks.lang.Batch;
import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarInteger;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;
import plugins.tprovoost.sequenceblocks.importer.PositionedSequenceFileImporter;

/**
 * Base abstract class for {@link SequenceFileImporter} batch loop.
 * 
 * @author Stephane
 */
public abstract class SequenceFileImporterBatch extends Batch implements SequenceBlock, PluginLibrary, PluginBundled
{
    // important to not initialize and create them in getBatchSource() and getBatchElement()
    protected Var<PositionedSequenceFileImporter> positionedImporter;
    protected Var<PositionedSequenceFileImporter> element;
    protected VarInteger series;

    // internal
    protected int limit;

    @Override
    public Var<PositionedSequenceFileImporter> getBatchSource()
    {
        // initialize variable if needed
        if (positionedImporter == null)
            positionedImporter = new Var<PositionedSequenceFileImporter>("Importer",
                    PositionedSequenceFileImporter.class);

        return positionedImporter;
    }

    @Override
    public Var<PositionedSequenceFileImporter> getBatchElement()
    {
        // initialize element if needed
        if (element == null)
            element = new Var<PositionedSequenceFileImporter>("Loop importer", PositionedSequenceFileImporter.class);

        return element;
    }

    @Override
    public void initializeLoop()
    {
        final PositionedSequenceFileImporter value = positionedImporter.getValue();

        if (value == null)
            throw new VarException(positionedImporter, "Input importer is null !");

        final SequenceFileImporter imp = value.importer;

        if (imp.getOpened() == null)
            throw new VarException(positionedImporter, "Importer is not opened !");

        // create new positioned importer for element initialization
        final PositionedSequenceFileImporter pi = new PositionedSequenceFileImporter(value);

        final int s = series.getValue().intValue();

        // defined series ? --> set series position
        if (s != -1)
            pi.s = s;

        // init element with a copy of current positioned importer (and eventually set serie position)
        element.setValue(pi);
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        super.declareInput(inputMap);

        // lazy creation
        if (series == null)
            series = new VarInteger("Series", -1);

        inputMap.add("series", series);
    }

    @Override
    public void declareLoopVariables(List<Var<?>> loopVariables)
    {
        // need to be called after
        super.declareLoopVariables(loopVariables);

        // lazy creation
        if (series == null)
            series = new VarInteger("Series", -1);

        loopVariables.add(series);
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
