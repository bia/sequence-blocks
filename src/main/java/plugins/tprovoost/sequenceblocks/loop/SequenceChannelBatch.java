package plugins.tprovoost.sequenceblocks.loop;

import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.sequence.Sequence;
import icy.sequence.SequenceUtil;
import plugins.adufour.blocks.lang.Batch;
import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * Simple batch loop to iterate over all C channel from an input Sequence.
 * 
 * @author Stephane
 */
public class SequenceChannelBatch extends Batch implements SequenceBlock, PluginLibrary, PluginBundled
{
    // important to not initialize here (even with null) and create them in getBatchSource() and getBatchElement()
    protected VarSequence inputSequence;
    protected VarSequence element;

    @Override
    public VarSequence getBatchSource()
    {
        // initialize variable if needed
        if (inputSequence == null)
            inputSequence = new VarSequence("Sequence", null);

        return inputSequence;
    }

    @Override
    public VarSequence getBatchElement()
    {
        // initialize element if needed
        if (element == null)
            element = new VarSequence("Channel Sequence", null);

        return element;
    }

    @Override
    public void initializeLoop()
    {
        final Sequence value = inputSequence.getValue();

        if (value == null)
            throw new VarException(inputSequence, "No input sequence indicated");
    }

    @Override
    public void beforeIteration()
    {
        try
        {
            // set result in element
            element.setValue(SequenceUtil.extractChannel(inputSequence.getValue(), getIterationCounter().getValue().intValue()));
        }
        catch (InterruptedException e)
        {
            // nothing to do
        }
    }

    @Override
    public boolean isStopConditionReached()
    {
        return getIterationCounter().getValue().intValue() >= inputSequence.getValue().getSizeC();
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
