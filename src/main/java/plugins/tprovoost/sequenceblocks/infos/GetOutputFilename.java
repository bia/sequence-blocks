/**
 * 
 */
package plugins.tprovoost.sequenceblocks.infos;

import icy.file.FileUtil;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.sequence.Sequence;
import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarBoolean;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.lang.VarString;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * Returns the output filename generated from origin filename and internal partitioning information
 * 
 * @author Stephane
 */
public class GetOutputFilename extends Plugin implements SequenceBlock, PluginBundled
{
    final protected VarSequence varSequence = new VarSequence("Sequence", null);
    final protected VarString outFilename = new VarString("Output filename", "");
    final protected VarBoolean withFolder = new VarBoolean("Folder", Boolean.TRUE);
    final protected VarBoolean withExtension = new VarBoolean("Extension", Boolean.TRUE);

    @Override
    public void run()
    {
        Sequence s = varSequence.getValue();
        if (s == null)
            throw new VarException(varSequence, "Sequence is null");

        String result = s.getOutputFilename(withExtension.getValue().booleanValue());

        if (!withFolder.getValue().booleanValue())
            result = FileUtil.getFileName(result);

        outFilename.setValue(result);
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("sequence", varSequence);
        inputMap.add("folder", withFolder);
        inputMap.add("extension", withExtension);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("outFilename", outFilename);
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}