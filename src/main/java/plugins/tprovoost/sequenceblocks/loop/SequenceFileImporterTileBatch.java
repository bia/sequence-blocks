package plugins.tprovoost.sequenceblocks.loop;

import java.awt.Rectangle;
import java.util.List;

import icy.file.SequenceFileImporter;
import icy.image.ImageUtil;
import icy.sequence.MetaDataUtil;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarInteger;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.importer.PositionedSequenceFileImporter;

/**
 * Simple batch loop to iterate over all XY tile from the specified opened {@link SequenceFileImporter} object.
 * 
 * @author Stephane
 */
public class SequenceFileImporterTileBatch extends SequenceFileImporterBatch
{
    // important to not initialize here (even with null) and create them in getBatchSource() and getBatchElement()
    protected VarInteger tileW;
    protected VarInteger tileH;

    protected List<Rectangle> tiles;

    @Override
    public void initializeLoop()
    {
        super.initializeLoop();

        // can take position from element which should have been initialized here
        final PositionedSequenceFileImporter pi = element.getValue();

        // can iterate over XY dimension ?
        if (pi.xyRegion == null)
        {
            final SequenceFileImporter imp = pi.importer;
            final int s = (pi.s == -1) ? 0 : pi.s;
            final int sizeX;
            final int sizeY;
            int tw;
            int th;

            try
            {
                sizeX = MetaDataUtil.getSizeX(pi.getMetadata(), s);
                sizeY = MetaDataUtil.getSizeY(pi.getMetadata(), s);

                tw = tileW.getValue().intValue();
                th = tileH.getValue().intValue();
                // auto ? --> get it from importer
                if (tw == -1)
                    tw = imp.getTileWidth(s);
                // auto ? --> get it from importer
                if (th == -1)
                    th = imp.getTileHeight(s);
            }
            catch (Exception e)
            {
                throw new VarException(element,
                        "Error while initializing SequenceFileImporter tile batch: " + e.getMessage());
            }

            // any size supported ? --> use 1024 by default
            if (tw == -1)
                tw = 1024;
            // tile loading not supported ? --> use full width
            else if (tw == 0)
                tw = sizeX;
            // any size supported ? --> use 1024 by default
            if (th == -1)
                th = 1024;
            // tile loading not supported ? --> use full height
            else if (th == 0)
                th = sizeY;

            // get tiles
            tiles = ImageUtil.getTileList(sizeX, sizeY, tw, th);
            // set limit
            limit = tiles.size();
        }
        else
            limit = 1;
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        super.declareInput(inputMap);
        
        // lazy creation
        if (tileW == null)
            tileW = new VarInteger("Tile width (-1 = auto)", -1);
        if (tileH == null)
            tileH = new VarInteger("Tile height (-1 = auto)", -1);

        inputMap.add("tileWidht", tileW);
        inputMap.add("tileHeight", tileH);
    }

    @Override
    public void declareLoopVariables(List<Var<?>> loopVariables)
    {
        // need to be called after
        super.declareLoopVariables(loopVariables);

        // lazy creation
        if (tileW == null)
            tileW = new VarInteger("Tile width (-1 = auto)", -1);
        if (tileH == null)
            tileH = new VarInteger("Tile height (-1 = auto)", -1);

        loopVariables.add(tileW);
        loopVariables.add(tileH);
    }

    @Override
    public void beforeIteration()
    {
        // set current position XY region
        if (positionedImporter.getValue().xyRegion == null)
        {
            element.getValue().xyRegion = tiles.get(getIterationCounter().getValue().intValue());
            // force element changed event so loop get correctly executed
            element.valueChanged(element, element.getValue(), element.getValue());
        }
    }

    @Override
    public boolean isStopConditionReached()
    {
        return getIterationCounter().getValue().intValue() >= limit;
    }
}
