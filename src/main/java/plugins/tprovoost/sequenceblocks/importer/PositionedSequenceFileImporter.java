/**
 * 
 */
package plugins.tprovoost.sequenceblocks.importer;

import java.awt.Rectangle;
import java.io.IOException;

import icy.common.exception.UnsupportedFormatException;
import icy.file.SequenceFileImporter;
import icy.sequence.MetaDataUtil;
import ome.xml.meta.OMEXMLMetadata;

/**
 * This class allow to use a {@link SequenceFileImporter} while storing a current position information to make its usage
 * more convenient with Protocols.
 * 
 * @author Stephane
 */
public class PositionedSequenceFileImporter
{
    public SequenceFileImporter importer;
    protected OMEXMLMetadata metadata;
    public int s;
    public int t;
    public int z;
    public int c;
    public Rectangle xyRegion;

    public PositionedSequenceFileImporter(SequenceFileImporter importer, OMEXMLMetadata metadata, int s, int t, int z,
            int c, Rectangle xyRegion)
    {
        super();

        this.importer = importer;
        this.xyRegion = xyRegion;
        this.metadata = metadata;
        this.s = s;
        this.t = t;
        this.z = z;
        this.c = c;
    }

    public PositionedSequenceFileImporter(SequenceFileImporter importer)
    {
        this(importer, null, -1, -1, -1, -1, null);
    }

    public PositionedSequenceFileImporter(PositionedSequenceFileImporter positionedImporter)
    {
        this(positionedImporter.importer, positionedImporter.metadata, positionedImporter.s, positionedImporter.t,
                positionedImporter.z, positionedImporter.c, positionedImporter.xyRegion);
    }

    public OMEXMLMetadata getMetadata() throws UnsupportedFormatException, IOException, InterruptedException
    {
        // not yet defined --> take it from importer if possible
        if ((metadata == null) && (importer.getOpened() != null))
        {
            metadata = importer.getOMEXMLMetaData();
            // clean the metadata
            MetaDataUtil.clean(metadata);
        }

        return metadata;
    }

    @Override
    public String toString()
    {
        String result;

        if (importer != null)
            result = importer.getClass().getSimpleName();
        else
            result = "No importer";

        if (s != -1)
            result += " S=" + s;
        if (t != -1)
            result += " T=" + t;
        if (z != -1)
            result += " Z=" + z;
        if (c != -1)
            result += " C=" + c;
        if (xyRegion != null)
            result += " XY=" + xyRegion;

        return result;
    }
}
