package plugins.tprovoost.sequenceblocks.importer;

import java.awt.Rectangle;

import icy.file.SequenceFileImporter;
import icy.image.IcyBufferedImage;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.roi.ROI;
import plugins.adufour.blocks.tools.io.IOBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarInteger;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * Block to returns an image from the given parameters and opened SequenceFileImporter.
 * 
 * @author Stephane
 */
public class SequenceFileImporterGetImage extends Plugin implements IOBlock, PluginLibrary, PluginBundled
{
    final protected Var<PositionedSequenceFileImporter> importer;
    final protected VarInteger series;
    final protected VarInteger resolution;
    final protected VarROIArray region;
    final protected VarInteger zIndex;
    final protected VarInteger tIndex;
    final protected VarInteger cIndex;

    final protected Var<IcyBufferedImage> image;

    public SequenceFileImporterGetImage()
    {
        super();

        importer = new Var<PositionedSequenceFileImporter>("Importer", PositionedSequenceFileImporter.class);
        series = new VarInteger("Series", -1);
        resolution = new VarInteger("Resolution (0=full, 1=1/2, ..)", 0);
        region = new VarROIArray("XY region (ROI)", null);
        zIndex = new VarInteger("Z (slice) index", -1);
        tIndex = new VarInteger("T (frame) index", -1);
        cIndex = new VarInteger("C (channel) index", -1);

        image = new Var<IcyBufferedImage>("Image", IcyBufferedImage.class);
    }

    @Override
    public void run()
    {
        final PositionedSequenceFileImporter pi = importer.getValue();

        if (pi == null)
            throw new VarException(importer, "Importer is null !");

        final SequenceFileImporter imp = pi.importer;

        if (imp.getOpened() == null)
            throw new VarException(importer, "Importer is not opened !");

        final int res = resolution.getValue().intValue();
        final ROI[] rois = region.getValue();

        int s = series.getValue().intValue();
        int z = zIndex.getValue().intValue();
        int t = tIndex.getValue().intValue();
        int c = cIndex.getValue().intValue();
        Rectangle rect = ((rois != null) && (rois.length > 0)) ? rois[0].getBounds5D().toRectangle2D().getBounds()
                : null;

        // undefined values ? use internal position if any defined
        if ((s == -1) && (pi.s != -1))
            s = pi.s;
        if ((z == -1) && (pi.z != -1))
            z = pi.z;
        if ((t == -1) && (pi.t != -1))
            t = pi.t;
        if ((c == -1) && (pi.c != -1))
            c = pi.c;
        if ((rect == null) && (pi.xyRegion != null))
            rect = pi.xyRegion;
        
        // still undefined ? --> set default value for series
        if (s == -1)
            s = 0;        

        try
        {
            image.setValue(imp.getImage(s, res, rect, z, t, c));
        }
        catch (Exception e)
        {
            throw new VarException(importer, e.getMessage());
        }
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("importer", importer);
        inputMap.add("series", series);
        inputMap.add("resolution", resolution);
        inputMap.add("region", region);
        inputMap.add("zIndex", zIndex);
        inputMap.add("tIndex", tIndex);
        inputMap.add("cIndex", cIndex);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("image", image);
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
