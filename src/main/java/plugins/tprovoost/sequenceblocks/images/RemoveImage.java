package plugins.tprovoost.sequenceblocks.images;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.sequence.Sequence;
import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarInteger;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * Remove an image from the Sequence.
 * 
 * @author thomasprovoost
 */
public class RemoveImage extends Plugin implements SequenceBlock, PluginLibrary, PluginBundled
{
    VarSequence varSeq = new VarSequence("Sequence", null);
    VarInteger imgIdxT = new VarInteger("T", 0);
    VarInteger imgIdxZ = new VarInteger("Z", 0);

    @Override
    public void run()
    {
        Sequence s = varSeq.getValue();
        if (s == null)
            throw new VarException(varSeq, "Input sequence is null !");

        s.removeImage(imgIdxT.getValue().intValue(), imgIdxZ.getValue().intValue());
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("sequence", varSeq);
        inputMap.add("Idx T", imgIdxT);
        inputMap.add("Idx Z", imgIdxZ);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        //
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
