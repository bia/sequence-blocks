package plugins.tprovoost.sequenceblocks.convert;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.sequence.Sequence;
import icy.sequence.SequenceUtil;
import icy.type.DataType;
import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarBoolean;
import plugins.adufour.vars.lang.VarEnum;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * @author thomasprovoost
 */
public class ConvertType extends Plugin implements SequenceBlock, PluginLibrary, PluginBundled
{
    final protected VarEnum<DataType> type = new VarEnum<DataType>("Type Wanted", DataType.UBYTE);
    final protected VarSequence inputSequence = new VarSequence("sequence", null);
    final protected VarSequence outputSequence = new VarSequence("converted", null);
    final protected VarBoolean rescale = new VarBoolean("Rescale", Boolean.TRUE);

    @Override
    public void run()
    {
        Sequence s = inputSequence.getValue();
        if (s == null)
            throw new VarException(inputSequence, "Input sequence is null.");

        try
        {
            outputSequence.setValue(SequenceUtil.convertToType(s, type.getValue(), rescale.getValue().booleanValue()));
        }
        catch (InterruptedException e)
        {
            // nothing to do
        }
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("sequence", inputSequence);
        inputMap.add("Type Wanted", type);
        inputMap.add("Rescale", rescale);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("converted", outputSequence);
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }

}
