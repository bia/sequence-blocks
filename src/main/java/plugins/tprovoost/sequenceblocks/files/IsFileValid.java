package plugins.tprovoost.sequenceblocks.files;

import java.io.File;

import icy.file.Loader;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarBoolean;
import plugins.adufour.vars.lang.VarMutable;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * Block to know if a file represents an image file
 * 
 * @author Stephane
 */
public class IsFileValid extends Plugin implements PluginLibrary, SequenceBlock, PluginBundled
{
    final protected VarMutable f_in = new VarMutable("File", null)
    {
        @Override
        public boolean isAssignableFrom(@SuppressWarnings("rawtypes") Var source)
        {
            return String.class == source.getType() || File.class == source.getType();
        }
    };
    final protected VarBoolean result = new VarBoolean("Valid", Boolean.FALSE);

    @Override
    public void run()
    {
        final Object obj = f_in.getValue();
        if (obj != null)
        {
            final File f;

            if (obj instanceof String)
                f = new File((String) obj);
            else
                f = (File) obj;

            result.setValue(Boolean.valueOf(Loader.isSupportedImageFile(f.getAbsolutePath())));
        }
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("file", f_in);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("valid", result);
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
