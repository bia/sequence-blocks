package plugins.tprovoost.sequenceblocks.creation;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.sequence.Sequence;
import icy.sequence.SequenceUtil;
import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarBoolean;
import plugins.adufour.vars.lang.VarIntegerArrayNative;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * @author thomasprovoost
 */
public class CombineChannels extends Plugin implements SequenceBlock, PluginLibrary, PluginBundled
{
    final protected VarSequence inputSequence = new VarSequence("Sequence 1", null);
    final protected VarSequence inputSequence2 = new VarSequence("Sequence 2", null);
    final protected VarSequence outputSequence = new VarSequence("Merged", null);
    final protected VarIntegerArrayNative channelIdx = new VarIntegerArrayNative("Channel(s) 1", new int[] {0});
    final protected VarIntegerArrayNative channelIdx2 = new VarIntegerArrayNative("Channel(s) 2", new int[] {0});
    final protected VarBoolean fillStackHole = new VarBoolean("Fill stack hole", Boolean.TRUE);
    final protected VarBoolean fitToMaxSize = new VarBoolean("Fit to max size", Boolean.TRUE);

    @Override
    public void run()
    {
        Sequence s = inputSequence.getValue();
        Sequence s2 = inputSequence2.getValue();

        if (s == null)
            throw new VarException(inputSequence, "Input sequence 1 is null.");
        if (s2 == null)
            throw new VarException(inputSequence2, "Input sequence 2 is null.");

        int[] channels1 = channelIdx.getValue();
        int[] channels2 = channelIdx2.getValue();
        int sizeC1 = channels1.length;
        int sizeC2 = channels2.length;

        // Because of concatC, it is necessary to create a sequence[] and int[] of the same size,
        // and duplicate the sequence as many times in the first as necessary.
        Sequence[] sequences = new Sequence[sizeC1 + sizeC2];
        int channels[] = new int[sizeC1 + sizeC2];

        for (int i = 0; i < sizeC1; ++i)
        {
            sequences[i] = s;
            channels[i] = channels1[i];
        }
        for (int i = 0; i < sizeC2; ++i)
        {
            sequences[sizeC1 + i] = s2;
            channels[sizeC1 + i] = channels2[i];
        }

        final Sequence res = SequenceUtil.concatC(sequences, channels, fillStackHole.getValue().booleanValue(),
                fitToMaxSize.getValue().booleanValue(), null);

        outputSequence.setValue(res);
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("sequence 1", inputSequence);
        inputMap.add("Channel 1", channelIdx);
        inputMap.add("sequence 2", inputSequence2);
        inputMap.add("Channel 2", channelIdx2);
        inputMap.add("fit to max size", fitToMaxSize);
        inputMap.add("fill stack hole", fillStackHole);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("out", outputSequence);
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
