package plugins.tprovoost.sequenceblocks.convert;

import java.awt.image.BufferedImage;

import icy.image.IcyBufferedImageUtil;
import icy.image.lut.LUT;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.sequence.Sequence;
import icy.util.OMEUtil;
import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarEnum;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * Block to render a given Sequence using a specific LUT.<br>
 * The result can be provided ARGB, RGB or GRAY sequence.
 * 
 * @author Stephane
 */
public class ConvertColor extends Plugin implements SequenceBlock, PluginBundled
{
    public static enum ColorConversion
    {
        GRAY, RGB, ARGB;
    };

    final protected VarSequence EZseq = new VarSequence("Sequence", null);
    final protected VarEnum<ColorConversion> EZtype = new VarEnum<ColorConversion>("Conversion", ColorConversion.ARGB);
    final protected Var<LUT> EZlut = new Var<LUT>("Lut", LUT.class);
    final protected VarSequence varOut = new VarSequence("Out", null);

    @Override
    public void run()
    {
        Sequence s;

        // EZ PLUG
        s = EZseq.getValue();
        if (s == null)
            throw new VarException(EZseq, "Input sequence is null.");

        try
        {
            varOut.setValue(convertColor(s, getImageType(EZtype.getValue()), EZlut.getValue()));
        }
        catch (InterruptedException e)
        {
            // nothing to do
        }
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("Sequence", EZseq);
        inputMap.add("Conversion", EZtype);
        inputMap.add("Lut", EZlut);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("Out", varOut);
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }

    public static int getImageType(ColorConversion value)
    {
        switch (value)
        {
            default:
            case ARGB:
                return BufferedImage.TYPE_INT_ARGB;

            case RGB:
                return BufferedImage.TYPE_INT_RGB;

            case GRAY:
                return BufferedImage.TYPE_BYTE_GRAY;
        }
    }

    public static Sequence convertColor(Sequence source, int imageType, LUT lut) throws IllegalArgumentException, InterruptedException
    {
        final Sequence result = new Sequence(OMEUtil.createOMEXMLMetadata(source.getOMEXMLMetadata()));
        // image receiver
        final BufferedImage imgOut = new BufferedImage(source.getSizeX(), source.getSizeY(), imageType);

        result.beginUpdate();
        try
        {
            for (int t = 0; t < source.getSizeT(); t++)
                for (int z = 0; z < source.getSizeZ(); z++)
                    result.setImage(t, z, IcyBufferedImageUtil.toBufferedImage(source.getImage(t, z), imgOut, lut));

            // rename channels and set final name
            switch (imageType)
            {
                default:
                case BufferedImage.TYPE_INT_ARGB:
                    result.setChannelName(0, "red");
                    result.setChannelName(1, "green");
                    result.setChannelName(2, "blue");
                    result.setChannelName(3, "alpha");
                    result.setName(source.getName() + " (ARGB rendering)");
                    break;

                case BufferedImage.TYPE_INT_RGB:
                    result.setChannelName(0, "red");
                    result.setChannelName(1, "green");
                    result.setChannelName(2, "blue");
                    result.setName(source.getName() + " (RGB rendering)");
                    break;

                case BufferedImage.TYPE_BYTE_GRAY:
                    result.setChannelName(0, "gray");
                    result.setName(source.getName() + " (gray rendering)");
                    break;
            }
        }
        finally
        {
            result.endUpdate();
        }

        return result;
    }
}
