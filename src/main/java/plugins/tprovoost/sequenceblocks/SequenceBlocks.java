package plugins.tprovoost.sequenceblocks;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginLibrary;

/**
 * This class is used as a main class for all the blocks contained in this
 * package. Each Block has a specified dedicated purpose. More documentation on
 * the concerned files.
 * 
 * @author thomasprovoost
 */
public class SequenceBlocks extends Plugin implements PluginLibrary
{
    //
}
