package plugins.tprovoost.sequenceblocks.extract;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.sequence.Sequence;
import icy.sequence.SequenceUtil;
import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarInteger;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * @author thomasprovoost
 */
public class ExtractChannel extends Plugin implements SequenceBlock, PluginLibrary, PluginBundled
{
    final protected VarSequence inputSequence = new VarSequence("Sequence", null);
    final protected VarSequence outputSequence = new VarSequence("Extracted", null);
    final protected VarInteger channelIdx = new VarInteger("Channel", 0);

    @Override
    public void run()
    {
        final Sequence s = inputSequence.getValue();
        final int channel = channelIdx.getValue().intValue();

        if (s == null)
            throw new VarException(inputSequence, "Input sequence is null.");
        if (channel < 0 || channel >= s.getSizeC())
            throw new VarException(channelIdx, "Channel index must be between 0 and " + (s.getSizeC() - 1));

        try
        {
            final Sequence res = SequenceUtil.extractChannel(s, channelIdx.getValue().intValue());
            res.setName(s.getName() + " - channel: " + channelIdx.getValue());

            outputSequence.setValue(res);
        }
        catch (InterruptedException e)
        {
            // nothing to do
        }
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("sequence", inputSequence);
        inputMap.add("channel", channelIdx);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("extracted", outputSequence);
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
