package plugins.tprovoost.sequenceblocks.files;

import java.io.File;

import icy.file.Loader;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.sequence.MetaDataUtil;
import ome.xml.meta.OMEXMLMetadata;
import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarInteger;
import plugins.adufour.vars.lang.VarMutable;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * Block to load Sequence metadata from a file
 * 
 * @author Stephane
 */
public class LoadMetadata extends Plugin implements SequenceBlock, PluginLibrary, PluginBundled
{
    final protected VarMutable f_in = new VarMutable("File", null)
    {
        @Override
        public boolean isAssignableFrom(@SuppressWarnings("rawtypes") Var source)
        {
            return (String.class == source.getType()) || (File.class == source.getType());
        }
    };
    final protected Var<OMEXMLMetadata> output = new Var<OMEXMLMetadata>("Metadata", OMEXMLMetadata.class);
    final protected VarInteger numSerie = new VarInteger("Series number", 1);

    @Override
    public void run()
    {
        final Object obj = f_in.getValue();
        if (obj != null)
        {
            final File f;

            if (obj instanceof String)
                f = new File((String) obj);
            else
                f = (File) obj;

            try
            {
                final OMEXMLMetadata meta = Loader.getOMEXMLMetaData(f.getAbsolutePath());

                if (meta != null)
                {
                    output.setValue(meta);
                    numSerie.setValue(Integer.valueOf(MetaDataUtil.getNumSeries(meta)));
                }
            }
            catch (Exception e)
            {
                throw new VarException(null, e.getMessage());
            }
        }
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("file", f_in);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("Metadata", output);
        outputMap.add("Serie number", numSerie);
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
