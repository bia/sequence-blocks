package plugins.tprovoost.sequenceblocks.infos;

import icy.math.UnitUtil;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.sequence.MetaDataUtil;
import ome.xml.meta.OMEXMLMetadata;
import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarDouble;
import plugins.adufour.vars.lang.VarInteger;
import plugins.adufour.vars.lang.VarString;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;
import plugins.tprovoost.sequenceblocks.files.LoadMetadata;

/**
 * Block to return main informations from a metadata object.
 * 
 * @see LoadMetadata
 * @author Stephane
 */
public class ReadMetadata extends Plugin implements SequenceBlock, PluginLibrary, PluginBundled
{
    final protected Var<OMEXMLMetadata> metadata = new Var<OMEXMLMetadata>("Metadata", OMEXMLMetadata.class);
    final protected VarInteger serie = new VarInteger("Series", 0);

    final protected VarInteger numSerie = new VarInteger("Num serie", 1);
    final protected VarString name = new VarString("Name", "");
    final protected VarInteger sizeX = new VarInteger("Size X", 1);
    final protected VarInteger sizeY = new VarInteger("Size Y", 1);
    final protected VarInteger sizeC = new VarInteger("Size C", 1);
    final protected VarInteger sizeZ = new VarInteger("Size Z", 1);
    final protected VarInteger sizeT = new VarInteger("Size T", 1);
    final protected VarDouble pxSizeX = new VarDouble("Pixel Size X (" + UnitUtil.MICRO_STRING + "m)", 1d);
    final protected VarDouble pxSizeY = new VarDouble("Pixel Size Y (" + UnitUtil.MICRO_STRING + "m)", 1d);
    final protected VarDouble pxSizeZ = new VarDouble("Pixel Size Z (" + UnitUtil.MICRO_STRING + "m)", 1d);
    final protected VarDouble timeIntT = new VarDouble("Time interval T (s)", 100d);
    final protected VarDouble positionX = new VarDouble("Position X (" + UnitUtil.MICRO_STRING + "m)", 0d);
    final protected VarDouble positionY = new VarDouble("Position Y (" + UnitUtil.MICRO_STRING + "m)", 0d);
    final protected VarDouble positionZ = new VarDouble("Position Z (" + UnitUtil.MICRO_STRING + "m)", 0d);

    @Override
    public void run()
    {
        final OMEXMLMetadata meta = metadata.getValue();
        if (meta == null)
            throw new VarException(metadata, "Metadata is null !");

        final int s = serie.getValue().intValue();
        final int numS = MetaDataUtil.getNumSeries(meta);

        numSerie.setValue(Integer.valueOf(numS));

        if (s >= numS)
            throw new VarException(serie, "Serie index must be between 0 and " + (numS - 1));

        name.setValue(MetaDataUtil.getName(meta, s));
        sizeX.setValue(Integer.valueOf(MetaDataUtil.getSizeX(meta, s)));
        sizeY.setValue(Integer.valueOf(MetaDataUtil.getSizeY(meta, s)));
        sizeZ.setValue(Integer.valueOf(MetaDataUtil.getSizeZ(meta, s)));
        sizeT.setValue(Integer.valueOf(MetaDataUtil.getSizeT(meta, s)));
        sizeC.setValue(Integer.valueOf(MetaDataUtil.getSizeC(meta, s)));
        pxSizeX.setValue(Double.valueOf(MetaDataUtil.getPixelSizeX(meta, s, pxSizeX.getDefaultValue().doubleValue())));
        pxSizeY.setValue(Double.valueOf(MetaDataUtil.getPixelSizeY(meta, s, pxSizeY.getDefaultValue().doubleValue())));
        pxSizeZ.setValue(Double.valueOf(MetaDataUtil.getPixelSizeZ(meta, s, pxSizeZ.getDefaultValue().doubleValue())));
        timeIntT.setValue(
                Double.valueOf(MetaDataUtil.getTimeInterval(meta, s, timeIntT.getDefaultValue().doubleValue())));
        // get position of first plane only for now
        positionX.setValue(Double.valueOf(MetaDataUtil.getPositionX(meta, s, 0, 0, 0, 0d)));
        positionY.setValue(Double.valueOf(MetaDataUtil.getPositionY(meta, s, 0, 0, 0, 0d)));
        positionZ.setValue(Double.valueOf(MetaDataUtil.getPositionZ(meta, s, 0, 0, 0, 0d)));
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("Metadata", metadata);
        inputMap.add("Serie", serie);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("numSerie", numSerie);
        outputMap.add("Name", name);
        outputMap.add("Size X", sizeX);
        outputMap.add("Size Y", sizeY);
        outputMap.add("Size C", sizeC);
        outputMap.add("Size Z", sizeZ);
        outputMap.add("Size T", sizeT);
        outputMap.add("Pixel Size X (mm)", pxSizeX);
        outputMap.add("Pixel Size Y (mm)", pxSizeY);
        outputMap.add("Pixel Size Z (mm)", pxSizeZ);
        outputMap.add("Time interval T (ms)", timeIntT);
        outputMap.add("positionx", positionX);
        outputMap.add("positiony", positionY);
        outputMap.add("positionz", positionZ);
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}