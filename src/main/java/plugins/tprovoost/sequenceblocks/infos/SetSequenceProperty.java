package plugins.tprovoost.sequenceblocks.infos;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.sequence.Sequence;
import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.lang.VarString;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * Block to set value (in String format) of a Sequence property (name, pixel size, custom...)<br>
 * Default metadata information can be set using specific property name:<br>
 * <ul>
 * <li><i>id</i> = sequence internal Id</li>
 * <li><i>name</i> = sequence name</li>
 * <li><i>positionX</i> = absolute position X of the image (in um)</li>
 * <li><i>positionY</i> = absolute position Y of the image (in um)</li>
 * <li><i>positionZ</i> = absolute position Z of the image (in um)</li>
 * <li><i>positionT</i> = acquisition date time</li>
 * <li><i>positionTOffset</i> = not available for property;</li>
 * <li><i>pixelSizeX</i> = pixel size X (in um)</li>
 * <li><i>pixelSizeY</i> = pixel size Y (in um)</li>
 * <li><i>pixelSizeZ</i> = pixel size Z (in um)</li>
 * <li><i>timeInterval</i> = time interval between each image (in second)</li>
 * <li><i>channelName</i> = not available for property</li>
 * <li><i>userName</i> = user who generated / acquired the dataset</li>
 * <li><i>virtual</i> = virtual mode enabled on the dataset</li>
 * </ul>
 * 
 * @author Stephane
 */
public class SetSequenceProperty extends Plugin implements SequenceBlock, PluginLibrary, PluginBundled
{
    protected VarSequence sequence = new VarSequence("Sequence", null);
    protected VarString propertyName = new VarString("Property", "name");
    protected VarString value = new VarString("Value", "");

    @Override
    public void run()
    {
        final Sequence seq = sequence.getValue();

        if (seq != null)
            seq.setProperty(propertyName.getValue(), value.getValue());
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("sequence", sequence);
        inputMap.add("property", propertyName);
        inputMap.add("value", value);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        //
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
