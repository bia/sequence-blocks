package plugins.tprovoost.sequenceblocks.infos;

import icy.image.colormap.LinearColorMap;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.sequence.Sequence;
import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.BlocksException;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarInteger;
import plugins.adufour.vars.lang.VarSequence;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * Block to retrieve a channel colormap from a given sequence
 * 
 * @author Stephane
 * @see SetColormap
 */
public class GetColormap extends Plugin implements SequenceBlock, PluginBundled
{
    private VarSequence sequenceIn = new VarSequence("Sequence", null);
    private VarInteger numChannel = new VarInteger("Channel", 0);
    private VarColormap colormap = new VarColormap("Color map", LinearColorMap.gray_);

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("input", sequenceIn);
        inputMap.add("numChannel", numChannel);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("output", colormap);
    }

    @Override
    public void run()
    {
        final Sequence seq = sequenceIn.getValue();

        if (seq != null)
        {
            final int ch = numChannel.getValue().intValue();

            if (ch < seq.getSizeC())
                colormap.setValue(seq.getColorMap(ch));
            else
                throw new BlocksException("Block 'Get Colormap': illegal channel number for this sequence !", true);
        }
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
