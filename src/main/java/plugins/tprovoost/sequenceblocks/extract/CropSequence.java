package plugins.tprovoost.sequenceblocks.extract;

import java.util.ArrayList;
import java.util.List;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.roi.ROI;
import icy.roi.ROIUtil;
import icy.sequence.Sequence;
import icy.sequence.SequenceUtil;
import icy.type.collection.CollectionUtil;
import icy.util.ShapeUtil.BooleanOperator;
import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarMutable;
import plugins.adufour.vars.lang.VarSequence;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * @author thomasprovoost
 */
public class CropSequence extends Plugin implements SequenceBlock, PluginLibrary, PluginBundled
{
    final protected VarSequence inputSequence = new VarSequence("Sequence", null);
    VarMutable inputROIs = new VarMutable("ROI(s)", null)
    {
        @Override
        public boolean isAssignableFrom(@SuppressWarnings("rawtypes") Var source)
        {
            return (ROI.class == source.getType()) || (ROI[].class == source.getType());
        }
    };
    final protected VarSequence outputSequence = new VarSequence("Cropped", null);

    @Override
    public void run()
    {
        outputSequence.setValue(null);

        final Sequence s = inputSequence.getValue();
        if (s == null)
            return;

        final Object obj = inputROIs.getValue();
        if (obj == null)
            return;

        final List<ROI> rois;

        if (obj instanceof ROI)
        {
            rois = new ArrayList<ROI>();
            rois.add((ROI) obj);
        }
        else
            rois = CollectionUtil.asList((ROI[]) obj);

        try
        {
            final ROI roi = ROIUtil.merge(rois, BooleanOperator.OR);

            outputSequence.setValue(SequenceUtil.getSubSequence(s, roi));
        }
        catch (InterruptedException e)
        {
            // nothing to do
        }
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("sequence", inputSequence);
        inputMap.add("roi", inputROIs);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("cropped", outputSequence);
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
