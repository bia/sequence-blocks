package plugins.tprovoost.sequenceblocks.loop;

import java.util.List;

import icy.file.SequenceFileImporter;
import icy.sequence.MetaDataUtil;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.importer.PositionedSequenceFileImporter;

/**
 * Simple batch loop to iterate over all series from the specified opened {@link SequenceFileImporter} object.
 * 
 * @author Stephane
 */
public class SequenceFileImporterSeriesBatch extends SequenceFileImporterBatch
{
    @Override
    public void initializeLoop()
    {
        super.initializeLoop();

        // can take position from element which should have been initialized here
        final PositionedSequenceFileImporter pi = element.getValue();

        try
        {
            // can iterate over S dimension ?
            if (pi.s == -1)
                limit = MetaDataUtil.getNumSeries(pi.getMetadata());
            else
                limit = 1;
        }
        catch (Exception e)
        {
            throw new VarException(element,
                    "Error while initializing SequenceFileImporter series batch: " + e.getMessage());
        }
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        super.declareInput(inputMap);

        // we don't want it here
        inputMap.remove(series);
    }

    @Override
    public void declareLoopVariables(List<Var<?>> loopVariables)
    {
        super.declareLoopVariables(loopVariables);

        // we don't want it here
        loopVariables.remove(series);
    }

    @Override
    public void beforeIteration()
    {
        // set current position S
        if (positionedImporter.getValue().s == -1)
        {
            element.getValue().s = getIterationCounter().getValue().intValue();
            // force element changed event so loop get correctly executed
            element.valueChanged(element, element.getValue(), element.getValue());
        }
    }

    @Override
    public boolean isStopConditionReached()
    {
        return getIterationCounter().getValue().intValue() >= limit;
    }
}
