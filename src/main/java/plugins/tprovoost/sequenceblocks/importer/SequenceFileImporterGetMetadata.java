package plugins.tprovoost.sequenceblocks.importer;

import icy.file.SequenceFileImporter;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.sequence.MetaDataUtil;
import ome.xml.meta.OMEXMLMetadata;
import plugins.adufour.blocks.tools.io.IOBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarInteger;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;
import plugins.tprovoost.sequenceblocks.infos.ReadMetadata;

/**
 * Block to returns the metadata from the specified opened SequenceFileImporter.
 * 
 * @see ReadMetadata
 * @author Stephane
 */
public class SequenceFileImporterGetMetadata extends Plugin implements IOBlock, PluginLibrary, PluginBundled
{
    final protected Var<PositionedSequenceFileImporter> importer;
    final protected Var<OMEXMLMetadata> metadata;
    final protected VarInteger numSeries;

    public SequenceFileImporterGetMetadata()
    {
        super();
        
        importer = new Var<PositionedSequenceFileImporter>("Importer", PositionedSequenceFileImporter.class);
        metadata = new Var<OMEXMLMetadata>("Metadata", OMEXMLMetadata.class);
        numSeries = new VarInteger("Series number", 1);
    }

    @Override
    public void run()
    {
        final PositionedSequenceFileImporter pi = importer.getValue();

        if (pi == null)
            throw new VarException(importer, "Importer is null !");

        final SequenceFileImporter imp = pi.importer;

        if (imp.getOpened() == null)
            throw new VarException(importer, "Importer is not opened !");

        try
        {
            final OMEXMLMetadata meta = imp.getOMEXMLMetaData();

            metadata.setValue(meta);

            if (meta != null)
                numSeries.setValue(Integer.valueOf(MetaDataUtil.getNumSeries(meta)));
        }
        catch (Exception e)
        {
            throw new VarException(importer, e.getMessage());
        }
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("importer", importer);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("metadata", metadata);
        outputMap.add("numSerie", numSeries);
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
