package plugins.tprovoost.sequenceblocks.extract;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.sequence.Sequence;
import icy.sequence.SequenceUtil;
import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarInteger;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * @author thomasprovoost
 */
public class ExtractSlice extends Plugin implements SequenceBlock, PluginLibrary, PluginBundled
{
    final protected VarSequence inputSequence = new VarSequence("Sequence", null);
    final protected VarSequence outputSequence = new VarSequence("Extracted", null);
    final protected VarInteger chosenZ = new VarInteger("Z", 0);

    @Override
    public void run()
    {
        Sequence s = inputSequence.getValue();
        if (s == null)
            throw new VarException(inputSequence, "Input sequence is null.");
        Sequence extracted = SequenceUtil.extractSlice(s, chosenZ.getValue().intValue());
        outputSequence.setValue(extracted);
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("sequence", inputSequence);
        inputMap.add("Z pos", chosenZ);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("extracted", outputSequence);
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
