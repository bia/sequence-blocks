package plugins.tprovoost.sequenceblocks.add;

import java.util.Arrays;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.roi.ROI;
import icy.sequence.Sequence;
import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarBoolean;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * Block to add one or several {@link ROI} to a Sequence
 * 
 * @author Stephane
 */
public class AddRois extends Plugin implements SequenceBlock, PluginLibrary, PluginBundled
{
    final protected VarSequence inputSequence = new VarSequence("Sequence", null);
    final protected VarROIArray rois = new VarROIArray("Roi(s)");
    final protected VarBoolean removePrevious = new VarBoolean("Remove previous", Boolean.FALSE);

    @Override
    public void run()
    {
        final Sequence s = inputSequence.getValue();
        if (s == null)
            throw new VarException(inputSequence, "Input sequence is null.");

        if (removePrevious.getValue().booleanValue())
            s.removeAllROI();

        final ROI[] r = rois.getValue();
        if (r != null)
            s.addROIs(Arrays.asList(r), false);
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("sequence", inputSequence);
        inputMap.add("rois(s)", rois);
        inputMap.add("remove", removePrevious);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        //
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
