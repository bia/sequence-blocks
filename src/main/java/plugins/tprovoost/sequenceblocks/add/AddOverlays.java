package plugins.tprovoost.sequenceblocks.add;

import icy.painter.Overlay;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.sequence.Sequence;
import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarArray;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * Block to add one or several {@link Overlay} to a Sequence
 * 
 * @author Stephane
 */
public class AddOverlays extends Plugin implements SequenceBlock, PluginLibrary, PluginBundled
{
    final protected VarSequence inputSequence = new VarSequence("Sequence", null);
    final protected VarArray<Overlay> overlays = new VarArray<Overlay>("Overlay(s)", Overlay[].class, new Overlay[0])
    {
        @Override
        public String getValueAsString()
        {
            final Overlay[] value = getValue();

            if (value == null || value.length == 0)
                return "No overlay";

            return value.length + " overlay";
        }
    };

    @Override
    public void run()
    {
        final Sequence s = inputSequence.getValue();
        if (s == null)
            throw new VarException(inputSequence, "Input sequence is null.");

        final Overlay[] o = overlays.getValue();
        if (o != null)
        {
            s.beginUpdate();
            try
            {
                for (Overlay overlay : o)
                    s.addOverlay(overlay);
            }
            finally
            {
                s.endUpdate();
            }
        }

    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("sequence", inputSequence);
        inputMap.add("overlay(s)", overlays);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        //
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
