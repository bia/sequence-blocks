package plugins.tprovoost.sequenceblocks.files;

import java.io.File;

import icy.file.Loader;
import icy.file.SequenceFileImporter;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.roi.ROI;
import plugins.adufour.blocks.tools.io.IOBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarBoolean;
import plugins.adufour.vars.lang.VarInteger;
import plugins.adufour.vars.lang.VarMutable;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * Block to returns a Sequence with the given parameters from the specified closed) SequenceFileImporter.
 * 
 * @author Stephane
 */
public class LoadSubSequence extends Plugin implements IOBlock, PluginLibrary, PluginBundled
{
    final protected Var<SequenceFileImporter> importer;
    final protected VarMutable file;
    final protected VarInteger series;
    final protected VarInteger resolution;
    final protected VarROIArray region;
    final protected VarInteger minZIndex;
    final protected VarInteger maxZIndex;
    final protected VarInteger minTIndex;
    final protected VarInteger maxTIndex;
    final protected VarInteger cIndex;
    final protected VarBoolean showProgress;

    final protected VarSequence sequence;

    public LoadSubSequence()
    {
        super();

        importer = new Var<SequenceFileImporter>("Importer", SequenceFileImporter.class);
        file = new VarMutable("File", null)
        {
            @Override
            public boolean isAssignableFrom(@SuppressWarnings("rawtypes") Var source)
            {
                return (String.class == source.getType()) || (File.class == source.getType());
            }
        };
        series = new VarInteger("Series", 0);
        resolution = new VarInteger("Resolution (0=full, 1=1/2, ..)", 0);
        region = new VarROIArray("XY region (ROI)", null);
        minZIndex = new VarInteger("Z min (slice)", -1);
        maxZIndex = new VarInteger("Z max (slice)", -1);
        minTIndex = new VarInteger("T min (frame)", -1);
        maxTIndex = new VarInteger("T max (frame)", -1);
        cIndex = new VarInteger("C (channel) index", -1);
        showProgress = new VarBoolean("Show progress", Boolean.FALSE);

        sequence = new VarSequence("Sequence", null);
    }

    @Override
    public void run()
    {
        final String path;
        final Object obj = file.getValue();

        if (obj != null)
        {
            if (obj instanceof String)
                path = (String) obj;
            else
                path = ((File) obj).getAbsolutePath();
        }
        else
            throw new VarException(file, "File is null !");

        // here importer can be null so we don't test for it
        final SequenceFileImporter imp = importer.getValue();
        final int s = series.getValue().intValue();
        final int r = resolution.getValue().intValue();
        final ROI[] rois = region.getValue();
        final ROI roi = ((rois != null) && (rois.length > 0)) ? rois[0] : null;
        final int minZ = minZIndex.getValue().intValue();
        final int maxZ = maxZIndex.getValue().intValue();
        final int minT = minTIndex.getValue().intValue();
        final int maxT = maxTIndex.getValue().intValue();
        final int c = cIndex.getValue().intValue();
        final boolean progress = showProgress.getValue().booleanValue();

        try
        {
            sequence.setValue(Loader.loadSequence(imp, path, s, r,
                    (roi != null) ? roi.getBounds5D().toRectangle2D().getBounds() : null, minZ, maxZ, minT, maxT, c,
                    false, progress));
        }
        catch (Exception e)
        {
            throw new VarException(importer, e.getMessage());
        }
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("file", file);
        inputMap.add("importer", importer);
        inputMap.add("series", series);
        inputMap.add("resolution", resolution);
        inputMap.add("region", region);
        inputMap.add("minZ", minZIndex);
        inputMap.add("maxZ", maxZIndex);
        inputMap.add("minT", minTIndex);
        inputMap.add("maxT", maxTIndex);
        inputMap.add("cIndex", cIndex);
        inputMap.add("showProgress", showProgress);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("sequence", sequence);
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
