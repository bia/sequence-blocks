package plugins.tprovoost.sequenceblocks.importer;

import icy.file.SequenceFileImporter;
import icy.image.IcyBufferedImage;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import plugins.adufour.blocks.tools.io.IOBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarInteger;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * Block to returns a thumbnail from the specified opened SequenceFileImporter.
 * 
 * @author Stephane
 */
public class SequenceFileImporterGetThumbnail extends Plugin implements IOBlock, PluginLibrary, PluginBundled
{
    final protected Var<PositionedSequenceFileImporter> importer;
    final protected VarInteger series;
    final protected Var<IcyBufferedImage> thumbnail;

    public SequenceFileImporterGetThumbnail()
    {
        super();

        importer = new Var<PositionedSequenceFileImporter>("Importer", PositionedSequenceFileImporter.class);
        series = new VarInteger("Series", 0);
        thumbnail = new Var<IcyBufferedImage>("Thumbnail", IcyBufferedImage.class);
    }

    @Override
    public void run()
    {
        final PositionedSequenceFileImporter pi = importer.getValue();

        if (pi == null)
            throw new VarException(importer, "Importer is null !");

        final SequenceFileImporter imp = pi.importer;

        if (imp.getOpened() == null)
            throw new VarException(importer, "Importer is not opened !");

        final int s = series.getValue().intValue();

        try
        {
            thumbnail.setValue(imp.getThumbnail(s));
        }
        catch (Exception e)
        {
            throw new VarException(importer, e.getMessage());
        }
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("importer", importer);
        inputMap.add("series", series);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("thumbnail", thumbnail);
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
