package plugins.tprovoost.sequenceblocks.convert;

import icy.image.IcyBufferedImageUtil.FilterType;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.sequence.Sequence;
import icy.sequence.SequenceUtil;

import javax.swing.SwingConstants;

import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarBoolean;
import plugins.adufour.vars.lang.VarEnum;
import plugins.adufour.vars.lang.VarInteger;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

public class Resize extends Plugin implements SequenceBlock, PluginBundled
{
    protected enum Proportional
    {
        NO, TO_WIDTH, TO_HEIGHT
    };

    final protected  VarSequence EZseq = new VarSequence("Sequence", null);
    final protected  VarInteger EZsizew = new VarInteger("Width", 320);
    final protected  VarInteger EZsizeh = new VarInteger("Height", 200);
    final protected  VarEnum<Proportional> EZproportional = new VarEnum<Proportional>("Proportional", Proportional.NO);
    final protected  VarBoolean EZscale = new VarBoolean("Scale content", Boolean.TRUE);
    final protected  VarEnum<FilterType> EZtype = new VarEnum<FilterType>("Filter", FilterType.BICUBIC);
    final protected  VarSequence varOut = new VarSequence("Out", null);

    @Override
    public void run()
    {
        Sequence s;
        int w, h;
        FilterType type;

        // EZ PLUG
        s = EZseq.getValue();
        w = EZsizew.getValue().intValue();
        h = EZsizeh.getValue().intValue();
        type = EZtype.getValue();

        if (s == null || w <= 0 || h <= 0)
            throw new VarException(null, "No sequence chosen for resize, or wrong size.");

        double scale = 1d * s.getWidth() / s.getHeight();
        switch (EZproportional.getValue())
        {
            case NO:
                break;
            case TO_WIDTH:
                h = (int) (w / scale);
                break;
            case TO_HEIGHT:
                w = (int) (scale * h);
                break;
        }

        if (EZscale.getValue().booleanValue())
            varOut.setValue(SequenceUtil.scale(s, w, h, type));
        else
            varOut.setValue(SequenceUtil.scale(s, w, h, false, SwingConstants.CENTER, SwingConstants.CENTER, type));
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("Sequence", EZseq);
        inputMap.add("Width", EZsizew);
        inputMap.add("Height", EZsizeh);
        inputMap.add("Proportional", EZproportional);
        inputMap.add("Filter", EZtype);
        inputMap.add("Scale content", EZscale);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("Out", varOut);
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
