package plugins.tprovoost.sequenceblocks.loop;

import java.util.List;

import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.roi.ROI;
import icy.sequence.Sequence;
import icy.sequence.SequenceUtil;
import plugins.adufour.blocks.lang.Batch;
import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * Simple batch loop to iterate over a set of Region (ROIs) from an input Sequence.
 * 
 * @author Stephane
 */
public class SequenceRegionBatch extends Batch implements SequenceBlock, PluginLibrary, PluginBundled
{
    // important to not initialize here (even with null) and create them in getBatchSource() and getBatchElement()
    protected VarSequence inputSequence;
    protected VarSequence element;
    protected VarROIArray rois;
    protected VarROIArray currentRoi;

    public SequenceRegionBatch()
    {
        super();
    }

    @Override
    public VarROIArray getBatchSource()
    {
        if (rois == null)
            rois = new VarROIArray("ROI(s)");

        return rois;
    }

    @Override
    public VarSequence getBatchElement()
    {
        if (element == null)
            element = new VarSequence("Region Sequence", null);

        return element;
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        super.declareInput(inputMap);

        if (inputSequence == null)
            inputSequence = new VarSequence("Sequence", null);

        inputMap.add(inputSequence.getName(), inputSequence);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        super.declareOutput(outputMap);

        if (currentRoi == null)
            currentRoi = new VarROIArray("Current ROI");

        outputMap.add(currentRoi.getName(), currentRoi);
    }

    @Override
    public void initializeLoop()
    {
        final Sequence value = inputSequence.getValue();

        if (value == null)
            throw new VarException(inputSequence, "No input sequence indicated");

        if (rois.getValue() == null)
            throw new VarException(rois, "No roi(s) indicated --> no region to iterate over");
    }

    @Override
    public void beforeIteration()
    {
        // set result in element
        final ROI roi = rois.getValue()[getIterationCounter().getValue().intValue()];

        try
        {
            element.setValue(SequenceUtil.getSubSequence(inputSequence.getValue(), roi));
            currentRoi.setValue(new ROI[] {roi});
        }
        catch (InterruptedException e)
        {
            // nothing to do
        }
    }

    @Override
    public boolean isStopConditionReached()
    {
        return getIterationCounter().getValue().intValue() >= rois.getValue().length;
    }

    @Override
    public void declareLoopVariables(List<Var<?>> loopVariables)
    {
        super.declareLoopVariables(loopVariables);
        loopVariables.add(inputSequence);
        loopVariables.add(currentRoi);
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
