package plugins.tprovoost.sequenceblocks.importer;

import icy.file.SequenceFileImporter;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import plugins.adufour.blocks.tools.io.IOBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarInteger;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * Block to returns the optimal tile size for region reading from the specified opened SequenceFileImporter. 
 * 
 * @author Stephane
 */
public class SequenceFileImporterGetTileSize extends Plugin implements IOBlock, PluginLibrary, PluginBundled
{
    final protected Var<PositionedSequenceFileImporter> importer;
    final protected VarInteger series;
    final protected VarInteger tileW;
    final protected VarInteger tileH;

    public SequenceFileImporterGetTileSize()
    {
        super();

        importer = new Var<PositionedSequenceFileImporter>("Importer", PositionedSequenceFileImporter.class);
        series = new VarInteger("Series", 0);
        tileW = new VarInteger("Tile width", 0);
        tileH = new VarInteger("Tile height", 0);
    }

    @Override
    public void run()
    {
        final PositionedSequenceFileImporter pi = importer.getValue();

        if (pi == null)
            throw new VarException(importer, "Importer is null !");

        final SequenceFileImporter imp = pi.importer;

        if (imp.getOpened() == null)
            throw new VarException(importer, "Importer is not opened !");
        
        final int s = series.getValue().intValue();

        try
        {
            tileW.setValue(Integer.valueOf(imp.getTileWidth(s)));
            tileH.setValue(Integer.valueOf(imp.getTileHeight(s)));
        }
        catch (Exception e)
        {
            throw new VarException(importer, e.getMessage());
        }
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("importer", importer);
        inputMap.add("series", series);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("tileW", tileW);
        outputMap.add("tileH", tileH);
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
