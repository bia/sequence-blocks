package plugins.tprovoost.sequenceblocks.infos;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.sequence.Sequence;
import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarInteger;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.lang.VarString;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * @author thomasprovoost
 */
public class GetChannelName extends Plugin implements SequenceBlock, PluginBundled
{
    private VarSequence varSequence = new VarSequence("Sequence", null);
    private VarInteger varIdx = new VarInteger("Channel", 0);
    private VarString varName = new VarString("Name", "");

    @Override
    public void run()
    {
        Sequence s = varSequence.getValue();
        if (s == null)
            throw new VarException(varSequence, "Sequence is null");

        int index = varIdx.getValue().intValue();
        if (index < 0 || index >= s.getSizeC())
            throw new VarException(varIdx, "Wrong channel index.");

        varName.setValue(s.getChannelName(index));
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("sequence", varSequence);
        inputMap.add("index", varIdx);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("name", varName);
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
