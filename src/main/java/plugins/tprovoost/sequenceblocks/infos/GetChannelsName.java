package plugins.tprovoost.sequenceblocks.infos;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.sequence.Sequence;

import java.util.ArrayList;
import java.util.List;

import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarArray;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * Block to retrieve all channels name from a given sequence
 * 
 * @author Stephane
 * @see GetChannelName
 * @see SetChannelsName
 */
public class GetChannelsName extends Plugin implements SequenceBlock, PluginBundled
{
    private VarSequence varSequence = new VarSequence("Sequence", null);
    private VarArray<String> varNames = new VarArray<String>("Names", String[].class, new String[0]);

    @Override
    public void run()
    {
        final Sequence s = varSequence.getValue();
        if (s == null)
            throw new VarException(varSequence, "Sequence is null");

        final List<String> names = new ArrayList<String>();

        for (int c = 0; c < s.getSizeC(); c++)
            names.add(s.getChannelName(c));

        varNames.setValue(names.toArray(new String[names.size()]));
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("sequence", varSequence);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("names", varNames);
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
