package plugins.tprovoost.sequenceblocks.importer;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import icy.file.Loader;
import icy.file.SequenceFileImporter;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.util.ClassUtil;
import plugins.adufour.blocks.tools.io.IOBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.gui.model.ValueSelectionModel;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarInteger;
import plugins.adufour.vars.lang.VarMutable;
import plugins.adufour.vars.lang.VarString;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * Block to open a file with a given SequenceFileImporter.
 * 
 * @author Stephane
 */
public class SequenceFileImporterOpen extends Plugin implements IOBlock, PluginLibrary, PluginBundled
{
    final static String ID_AUTO = "Automatic";

    final protected VarString importerName;
    final protected Var<PositionedSequenceFileImporter> importer;
    final protected VarMutable file;
    final protected VarInteger flag;

    // internal
    final protected Map<String, SequenceFileImporter> importersMap;

    public SequenceFileImporterOpen()
    {
        super();

        final List<String> importersName = new ArrayList<String>();
        final List<SequenceFileImporter> importers = Loader.getSequenceFileImporters();

        importerName = new VarString("Select importer", "", 1 + importers.size());
        // build importer map
        importersMap = new HashMap<String, SequenceFileImporter>();

        // automatic selection
        importersName.add(ID_AUTO);
        importersMap.put(ID_AUTO, null);
        for (SequenceFileImporter importer : importers)
        {
            String className = ClassUtil.getBaseClassName(importer.getClass().getName());
            final int ind = className.lastIndexOf('.');

            // just get final class name without package name
            if (ind > -1)
                className = className.substring(ind + 1, className.length());

            importersName.add(className);
            importersMap.put(className, importer);
        }

        // initialize importer selector field
        importerName.setDefaultEditorModel(new ValueSelectionModel<String>(
                importersName.toArray(new String[importersName.size()]), importersName.get(0), false));

        importer = new Var<PositionedSequenceFileImporter>("Importer", PositionedSequenceFileImporter.class);
        file = new VarMutable("File", null)
        {
            @SuppressWarnings("rawtypes")
            @Override
            public boolean isAssignableFrom(Var source)
            {
                return (String.class == source.getType()) || (File.class == source.getType());
            }
        };
        flag = new VarInteger("Flag", 0);
    }

    @SuppressWarnings("resource")
    @Override
    public void run()
    {
        final String path;
        final Object obj = file.getValue();

        if (obj != null)
        {
            if (obj instanceof String)
                path = (String) obj;
            else
                path = ((File) obj).getAbsolutePath();
        }
        else
            throw new VarException(file, "File is null !");

        // get selected importer
        SequenceFileImporter imp = importersMap.get(importerName.getValue());

        // automatic selection from input file ?
        if (imp == null)
            imp = Loader.getSequenceFileImporter(path, true);

        // cannot get importer ?
        if (imp == null)
            throw new VarException(importer, "Cannot find an importer for file: '" + path + "' !");

        // set importer
        importer.setValue(new PositionedSequenceFileImporter(imp));

        try
        {
            // try to open the importer
            imp.open(path, flag.getValue().intValue());
        }
        catch (Exception e)
        {
            throw new VarException(importer, e.getMessage());
        }
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("name", importerName);
        inputMap.add("file", file);
        inputMap.add("flag", flag);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("importer", importer);
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
