package plugins.tprovoost.sequenceblocks.loop;

import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

import icy.file.SequenceFileImporter;
import icy.roi.ROI;
import icy.sequence.MetaDataUtil;
import icy.type.rectangle.Rectangle2DUtil;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarInteger;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.importer.PositionedSequenceFileImporter;

/**
 * Simple batch loop to iterate over a set of XY region defined by ROIs from the specified opened
 * {@link SequenceFileImporter} object.
 * 
 * @author Stephane
 */
public class SequenceFileImporterRegionBatch extends SequenceFileImporterBatch
{
    // important to not initialize here (even with null) and create them in getBatchSource() and getBatchElement()
    protected VarROIArray rois;
    protected VarInteger inputRoisResolution;
    protected VarInteger inputRoisMargin;
    protected List<Rectangle> regions;

    @Override
    public void initializeLoop()
    {
        super.initializeLoop();

        // can take position from element which should have been initialized here
        final PositionedSequenceFileImporter pi = element.getValue();

        // can iterate over XY dimension ?
        if ((pi.xyRegion == null) && (rois.getValue() != null))
        {
            final int s = (pi.s == -1) ? 0 : pi.s;
            final Rectangle regionMask;

            try
            {
                // mask for region
                regionMask = new Rectangle(0, 0, MetaDataUtil.getSizeX(pi.getMetadata(), s),
                        MetaDataUtil.getSizeY(pi.getMetadata(), s));
            }
            catch (Exception e)
            {
                throw new VarException(element,
                        "Error while initializing SequenceFileImporter region batch: " + e.getMessage());
            }

            final double scaleFactor = Math.pow(2, inputRoisResolution.getValue().doubleValue());
            final double margeFactor = 1d + (inputRoisMargin.getValue().doubleValue() / 100d);

            // build 2D regions from ROI
            regions = new ArrayList<Rectangle>();
            for (ROI roi : rois.getValue())
            {
                Rectangle2D region = roi.getBounds5D().toRectangle2D();

                // convert to full resolution if needed
                if (scaleFactor != 1d)
                    region = Rectangle2DUtil.getScaledRectangle(region, scaleFactor, false, true);
                // apply marge factor if needed
                if (margeFactor != 1d)
                    region = Rectangle2DUtil.getScaledRectangle(region, margeFactor, true, false);

                // use regionMask to limit to image region
                regions.add(region.getBounds().intersection(regionMask));
            }

            // set limit
            limit = regions.size();
        }
        else
            limit = 1;
    }

    @Override
    public void beforeIteration()
    {
        // set current position XY region
        if (positionedImporter.getValue().xyRegion == null)
        {
            element.getValue().xyRegion = regions.get(getIterationCounter().getValue().intValue());
            // force element changed event so loop get correctly executed
            element.valueChanged(element, element.getValue(), element.getValue());
        }
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        super.declareInput(inputMap);

        // lazy creation
        if (rois == null)
            rois = new VarROIArray("XY regions (ROIs)");
        if (inputRoisResolution == null)
            inputRoisResolution = new VarInteger("ROIs resolution", 0);
        if (inputRoisMargin == null)
            inputRoisMargin = new VarInteger("ROIs margin", 20);

        inputMap.add("regions", rois);
        inputMap.add("inputResolution", inputRoisResolution);
        inputMap.add("inputRoisMarge", inputRoisMargin);
    }

    @Override
    public void declareLoopVariables(List<Var<?>> loopVariables)
    {
        super.declareLoopVariables(loopVariables);

        // lazy creation
        if (rois == null)
            rois = new VarROIArray("XY regions (ROIs)");
        if (inputRoisResolution == null)
            inputRoisResolution = new VarInteger("ROIs resolution", 0);
        if (inputRoisMargin == null)
            inputRoisMargin = new VarInteger("ROIs margin", 20);

        loopVariables.add(rois);
        loopVariables.add(inputRoisResolution);
        loopVariables.add(inputRoisMargin);
    }

    @Override
    public boolean isStopConditionReached()
    {
        return getIterationCounter().getValue().intValue() >= limit;
    }
}
