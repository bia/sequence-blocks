package plugins.tprovoost.sequenceblocks.creation;

import icy.image.IcyBufferedImage;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.sequence.Sequence;
import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarSequence;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * @author thomasprovoost
 */
public class CreateSequence extends Plugin implements SequenceBlock, PluginLibrary, PluginBundled
{
    final protected Var<IcyBufferedImage> in = new Var<IcyBufferedImage>("image (optional)", IcyBufferedImage.class);
    final protected VarSequence out = new VarSequence("sequence", null);

    @Override
    public void run()
    {
        out.setValue(new Sequence(in.getValue()));
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("image (optional)", in);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("sequence", out);
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
