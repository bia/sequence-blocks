package plugins.tprovoost.sequenceblocks.infos;

import icy.math.UnitUtil;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.sequence.Sequence;
import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarDouble;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * @author thomasprovoost
 */
public class SetResolution extends Plugin implements SequenceBlock, PluginBundled
{
    final protected  VarSequence varSequence = new VarSequence("Sequence", null);
    final protected  VarDouble pxSizeX = new VarDouble("Pixel Size X (" + UnitUtil.MICRO_STRING + "m)", 1d);
    final protected  VarDouble pxSizeY = new VarDouble("Pixel Size Y (" + UnitUtil.MICRO_STRING + "m)", 1d);
    final protected  VarDouble pxSizeZ = new VarDouble("Pixel Size Z (" + UnitUtil.MICRO_STRING + "m)", 1d);
    final protected  VarDouble timeIntT = new VarDouble("Time interval T (s)", 100d);

	@Override
	public void run()
	{
		Sequence s = varSequence.getValue();

		if (s != null)
		{
			double x = pxSizeX.getValue().doubleValue();
			double y = pxSizeY.getValue().doubleValue();
			double z = pxSizeZ.getValue().doubleValue();
			double ti = timeIntT.getValue().doubleValue();
			if (x != -1)
				s.setPixelSizeX(x);
			if (y != -1)
				s.setPixelSizeY(y);
			if (z != -1)
				s.setPixelSizeZ(z);
			if (ti != -1)
				s.setTimeInterval(ti);
		} else
		{
			throw new VarException(varSequence, "Sequence is null");
		}
	}

	@Override
	public void declareInput(VarList inputMap)
	{
		inputMap.add("sequence", varSequence);
		inputMap.add("PxSize X (" + UnitUtil.MICRO_STRING + "m)", pxSizeX);
		inputMap.add("PxSize Y (" + UnitUtil.MICRO_STRING + "m)", pxSizeY);
		inputMap.add("PxSize Z (" + UnitUtil.MICRO_STRING + "m)", pxSizeZ);
		inputMap.add("Time interval T (s)", timeIntT);
	}

	@Override
	public void declareOutput(VarList outputMap)
	{
	    //
	}

	@Override
	public String getMainPluginClassName()
	{
		return SequenceBlocks.class.getName();
	}
}
