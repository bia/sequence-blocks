package plugins.tprovoost.sequenceblocks.images;

import icy.image.IcyBufferedImage;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.sequence.Sequence;
import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * Add an image to the Sequence.
 * 
 * @author thomasprovoost
 */
public class AddImage extends Plugin implements SequenceBlock, PluginLibrary, PluginBundled
{
    final protected Var<IcyBufferedImage> in = new Var<IcyBufferedImage>("Image", IcyBufferedImage.class);
    final protected VarSequence varSeq = new VarSequence("Sequence", null);

    @Override
    public void run()
    {
        Sequence s = varSeq.getValue();
        if (s == null)
            throw new VarException(varSeq, "Input sequence is null.");
        s.addImage(in.getValue());
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("sequence", varSeq);
        inputMap.add("image", in);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        //
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
