package plugins.tprovoost.sequenceblocks.creation;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.sequence.Sequence;
import icy.sequence.SequenceUtil;
import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * @author thomasprovoost
 */
public class DuplicateSequence extends Plugin implements SequenceBlock, PluginLibrary, PluginBundled
{
    final protected VarSequence in = new VarSequence("sequence", null);
    final protected VarSequence out = new VarSequence("duplicated", null);

    @Override
    public void run()
    {
        Sequence s = in.getValue();
        if (s == null)
            throw new VarException(in, "Input Sequence is null.");

        try
        {
            out.setValue(SequenceUtil.getCopy(s));
        }
        catch (InterruptedException e)
        {
            // nothing to do
        }
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("sequence", in);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("duplicated", out);
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
