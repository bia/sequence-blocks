/**
 * 
 */
package plugins.tprovoost.sequenceblocks.op;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.roi.ROI;
import icy.roi.ROIUtil;
import icy.sequence.Sequence;
import icy.sequence.SequenceDataIterator;
import icy.type.DataIteratorUtil;
import icy.type.collection.CollectionUtil;
import icy.util.ShapeUtil.BooleanOperator;
import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarDouble;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.util.VarException;
import plugins.kernel.roi.roi5d.ROI5DStackRectangle;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * Block to fill area outside the ROI regions with the specified value
 * 
 * @author Stephane
 */
public class FillOuterSequence extends Plugin implements SequenceBlock, PluginLibrary, PluginBundled
{
    final protected VarSequence inputSequence = new VarSequence("Sequence", null);
    final protected VarROIArray rois = new VarROIArray("Roi(s)");
    final protected VarDouble fillValue = new VarDouble("Value", 0d);

    @Override
    public void run()
    {
        final Sequence sequence = inputSequence.getValue();
        if (sequence == null)
            throw new VarException(inputSequence, "Input sequence is null.");

        try
        {
            if (rois.getValue() != null)
            {
                try
                {
                    final ROI roiUnion = ROIUtil.merge(CollectionUtil.asList(rois.getValue()), BooleanOperator.OR);
                    final ROI roiSeq = new ROI5DStackRectangle(sequence.getBounds5D());
                    final ROI roi = roiSeq.getSubtraction(roiUnion);

                    final double value = fillValue.getValue().doubleValue();

                    DataIteratorUtil.set(new SequenceDataIterator(sequence, roi), value);

                    sequence.dataChanged();
                }
                catch (UnsupportedOperationException e)
                {
                    throw new VarException(rois, e.getMessage());
                }
            }
        }
        catch (InterruptedException e)
        {
            // nothing to do
        }
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("sequence", inputSequence);
        inputMap.add("rois", rois);
        inputMap.add("value", fillValue);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        //
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}