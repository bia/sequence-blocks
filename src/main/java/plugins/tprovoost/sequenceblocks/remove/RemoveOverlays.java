/**
 * 
 */
package plugins.tprovoost.sequenceblocks.remove;

import icy.painter.Overlay;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.sequence.Sequence;
import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarArray;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * Block to remove one or several {@link Overlay} from a {@link Sequence}
 * 
 * @author Stephane
 */
public class RemoveOverlays extends Plugin implements SequenceBlock, PluginLibrary, PluginBundled
{
    final protected VarSequence inputSequence = new VarSequence("Sequence", null);
    final protected VarArray<Overlay> overlays = new VarArray<Overlay>("Overlay(s)", Overlay[].class, new Overlay[0]);

    @Override
    public void run()
    {
        final Sequence s = inputSequence.getValue();
        if (s == null)
            throw new VarException(inputSequence, "Input sequence is null.");

        final Overlay[] o = overlays.getValue();
        if (o != null)
        {
            s.beginUpdate();
            try
            {
                for (Overlay overlay : o)
                    s.removeOverlay(overlay);
            }
            finally
            {
                s.endUpdate();
            }
        }
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("sequence", inputSequence);
        inputMap.add("overlay(s)", overlays);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        //
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
