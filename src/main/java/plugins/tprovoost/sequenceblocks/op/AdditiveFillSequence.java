/**
 * 
 */
package plugins.tprovoost.sequenceblocks.op;

import icy.image.ImageDataIterator;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.roi.ROI;
import icy.sequence.Sequence;
import icy.sequence.SequenceDataIterator;
import icy.type.DataIterator;
import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * Block to do additive fill of area inside the ROI regions with the specified value.<br>
 * The result of this operation is similar to heatmap production from ROIs.
 * 
 * @author Stephane
 */
public class AdditiveFillSequence extends Plugin implements SequenceBlock, PluginLibrary, PluginBundled
{
    final protected VarSequence inputSequence = new VarSequence("Sequence", null);
    final protected VarROIArray rois = new VarROIArray("Roi(s)");

    @Override
    public void run()
    {
        final Sequence sequence = inputSequence.getValue();
        if (sequence == null)
            throw new VarException(inputSequence, "Input sequence is null.");

        try
        {
            if (rois.getValue() != null)
            {
                for (ROI roi : rois.getValue())
                    doAdditiveFill(sequence, roi, 1d);

                sequence.dataChanged();
            }
        }
        catch (InterruptedException e)
        {
            // nothing to do
        }
    }

    public static void doAdditiveFill(Sequence sequence, ROI roi, double value) throws InterruptedException
    {
        doAdditiveFill(new SequenceDataIterator(sequence, roi), value);
    }

    public static void doAdditiveFill(DataIterator it, double value) throws InterruptedException
    {
        it.reset();

        while (!it.done())
        {
            it.set(it.get() + value);
            it.next();
        }

        try
        {
            // not really nice to do that here, but it's to preserve backward compatibility
            if (it instanceof SequenceDataIterator)
                ((SequenceDataIterator) it).flush();
            else if (it instanceof ImageDataIterator)
                ((ImageDataIterator) it).flush();
        }
        catch (Throwable t)
        {
            // ignore
        }
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("sequence", inputSequence);
        inputMap.add("rois", rois);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        //
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}