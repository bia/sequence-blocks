package plugins.tprovoost.sequenceblocks.infos;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.roi.ROI;
import icy.sequence.Sequence;
import icy.sequence.SequenceUtil;
import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarBoolean;
import plugins.adufour.vars.lang.VarEnum;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.util.VarException;
import plugins.kernel.roi.roi2d.ROI2DRectangle;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * Block to retrieve ROI(s) from a Sequence as 2D Rectangular regions.<br>
 * The <i>origin resolution</i> parameter indicate if we want to rescale ROI to the original image resolution in case we
 * have a Sequence which represents a sub resolution of the original image.
 * 
 * @author Stephane
 */
public class GetRoisAsRegion extends Plugin implements SequenceBlock, PluginLibrary, PluginBundled
{
    public static enum TypeSelection
    {
        ALL, SELECTED
    };

    final protected VarSequence inputSequence = new VarSequence("Sequence", null);
    final protected VarEnum<TypeSelection> type = new VarEnum<TypeSelection>("ROI(s) to get", TypeSelection.ALL);
    final protected VarBoolean originResolution = new VarBoolean("Origin resolution", Boolean.TRUE);
    final protected VarROIArray regionsRois = new VarROIArray("Region ROI(s)", null);

    @Override
    public void run()
    {
        final Sequence sequence = inputSequence.getValue();
        if (sequence == null)
            throw new VarException(inputSequence, "Input sequence is null.");

        final List<ROI> rois;
        final List<ROI2DRectangle> result = new ArrayList<ROI2DRectangle>();

        switch (type.getValue())
        {
            default:
                rois = sequence.getROIs();
                break;
            case SELECTED:
                rois = sequence.getSelectedROIs();
                break;
        }

        final boolean scale = originResolution.getValue().booleanValue();

        // build rectangular ROI(s)
        for (ROI roi : rois)
        {
            // get ROI 2D bounds
            Rectangle rect = roi.getBounds5D().toRectangle2D().getBounds();

            // scale if needed
            if (scale)
                rect = SequenceUtil.getOriginRectangle(rect, sequence);

            // add to result
            result.add(new ROI2DRectangle(rect));
        }

        regionsRois.setValue(result.toArray(new ROI[result.size()]));
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("sequence", inputSequence);
        inputMap.add("type", type);
        inputMap.add("originResolution", originResolution);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("regionsRois", regionsRois);
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
