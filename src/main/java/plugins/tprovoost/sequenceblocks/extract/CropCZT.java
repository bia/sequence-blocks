package plugins.tprovoost.sequenceblocks.extract;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.sequence.Sequence;
import icy.sequence.SequenceUtil;
import icy.type.rectangle.Rectangle5D;
import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarInteger;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

public class CropCZT extends Plugin implements SequenceBlock, PluginBundled
{
    final protected VarSequence varSeq = new VarSequence("Sequence", null);
    final protected VarInteger varStartC = new VarInteger("Start index C", 0);
    final protected VarInteger varSizeC = new VarInteger("Size C", 1);
    final protected VarInteger varStartZ = new VarInteger("Start index Z", 0);
    final protected VarInteger varSizeZ = new VarInteger("Size Z", 1);
    final protected VarInteger varStartT = new VarInteger("Start index T", 0);
    final protected VarInteger varSizeT = new VarInteger("Size T", 1);
    final protected VarSequence varOut = new VarSequence("Out", null);

    @Override
    public void run()
    {
        final Sequence seq;
        final int startX, sizeX;
        final int startY, sizeY;
        final int startC, sizeC;
        final int startZ, sizeZ;
        final int startT, sizeT;

        // EZ PLUG
        seq = varSeq.getValue();

        if (seq == null)
            throw new VarException(varSeq, "No sequence chosen !");

        startC = Math.min(varStartC.getValue().intValue(), seq.getSizeC() - 1);
        sizeC = Math.min(varSizeC.getValue().intValue(), seq.getSizeC() - startC);
        startZ = Math.min(varStartZ.getValue().intValue(), seq.getSizeZ() - 1);
        sizeZ = Math.min(varSizeZ.getValue().intValue(), seq.getSizeZ() - startZ);
        startT = Math.min(varStartT.getValue().intValue(), seq.getSizeT() - 1);
        sizeT = Math.min(varSizeT.getValue().intValue(), seq.getSizeT() - startT);

        startX = 0;
        sizeX = seq.getSizeX();
        startY = 0;
        sizeY = seq.getSizeY();

        if ((sizeC <= 0) || (sizeZ <= 0) || (sizeT <= 0))
            throw new VarException(null, "Size C/Z/T cannot be <= 0 !");

        varOut.setValue(SequenceUtil.getSubSequence(seq,
                new Rectangle5D.Integer(startX, startY, startZ, startT, startC, sizeX, sizeY, sizeZ, sizeT, sizeC)));
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("Sequence", varSeq);
        inputMap.add("Start index C", varStartC);
        inputMap.add("Size C", varSizeC);
        inputMap.add("Start index Z", varStartZ);
        inputMap.add("Size Z", varSizeZ);
        inputMap.add("Start index T", varStartT);
        inputMap.add("Size T", varSizeT);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("Out", varOut);
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
