package plugins.tprovoost.sequenceblocks.loop;

import icy.file.SequenceFileImporter;
import icy.sequence.MetaDataUtil;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.importer.PositionedSequenceFileImporter;

/**
 * Simple batch loop to iterate over all Z slice from specified opened {@link SequenceFileImporter} object.
 * 
 * @author Stephane
 */
public class SequenceFileImporterSliceBatch extends SequenceFileImporterBatch
{
    @Override
    public void initializeLoop()
    {
        super.initializeLoop();

        // can take position from element which should have been initialized here
        final PositionedSequenceFileImporter pi = element.getValue();
        
        try
        {
            // can iterate over Z dimension ?
            if (pi.z == -1)
                limit = MetaDataUtil.getSizeZ(pi.getMetadata(), (pi.s == -1) ? 0 : pi.s);
            else
                limit = 1;
        }
        catch (Exception e)
        {
            throw new VarException(element,
                    "Error while initializing SequenceFileImporter slice batch: " + e.getMessage());
        }
    }

    @Override
    public void beforeIteration()
    {
        // set current position Z
        if (positionedImporter.getValue().z == -1)
        {
            element.getValue().z = getIterationCounter().getValue().intValue();
            // force element changed event so loop get correctly executed
            element.valueChanged(element, element.getValue(), element.getValue());
        }
    }

    @Override
    public boolean isStopConditionReached()
    {
        return getIterationCounter().getValue().intValue() >= limit;
    }
}
