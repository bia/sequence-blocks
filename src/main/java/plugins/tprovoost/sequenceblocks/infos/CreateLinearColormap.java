package plugins.tprovoost.sequenceblocks.infos;

import icy.image.colormap.LinearColorMap;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;

import java.awt.Color;

import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarColor;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * Block to create a linear colormap from a color value
 * 
 * @author Stephane
 * @see SetColormap
 */
public class CreateLinearColormap extends Plugin implements SequenceBlock, PluginBundled
{
    private VarColor color = new VarColor("Color", Color.white);
    private VarColormap colormap = new VarColormap("Color map", LinearColorMap.gray_);

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("input", color);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("output", colormap);
    }

    @Override
    public void run()
    {
        colormap.setValue(new LinearColorMap("color", color.getValue()));
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
