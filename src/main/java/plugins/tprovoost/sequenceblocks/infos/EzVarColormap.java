package plugins.tprovoost.sequenceblocks.infos;

import icy.image.colormap.IcyColorMap;
import plugins.adufour.ezplug.EzVar;
import plugins.tprovoost.sequenceblocks.infos.VarColormap.ColormapSelectionModel;

public class EzVarColormap extends EzVar<IcyColorMap>
{
    /**
     * Constructs a new variable that handles colormap data.
     * 
     * @param varName
     *        the parameter name
     * @param defaultValue IcyColorMap
     */
    public EzVarColormap(String varName, IcyColorMap defaultValue)
    {
        super(new VarColormap(varName, defaultValue), new ColormapSelectionModel());
    }
}
