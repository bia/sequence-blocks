package plugins.tprovoost.sequenceblocks.infos;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.sequence.Sequence;
import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.lang.VarString;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * Returns the name of the Sequence object
 * 
 * @author Stephane
 */
public class GetName extends Plugin implements SequenceBlock, PluginBundled
{
    final protected VarSequence varSequence = new VarSequence("Sequence", null);
    final protected VarString name = new VarString("Name", "");

    @Override
    public void run()
    {
        Sequence s = varSequence.getValue();
        if (s == null)
            throw new VarException(varSequence, "Sequence is null");
        name.setValue(s.getName());
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("sequence", varSequence);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("name", name);
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
