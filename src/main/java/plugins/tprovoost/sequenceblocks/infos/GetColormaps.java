package plugins.tprovoost.sequenceblocks.infos;

import icy.image.colormap.IcyColorMap;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.sequence.Sequence;

import java.util.ArrayList;
import java.util.List;

import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarArray;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * Block to retrieve all channels colormap from a given sequence
 * 
 * @author Stephane
 * @see SetColormaps
 */
public class GetColormaps extends Plugin implements SequenceBlock, PluginBundled
{
    private VarSequence sequenceIn = new VarSequence("Sequence", null);
    private VarArray<IcyColorMap> colormaps = new VarArray<IcyColorMap>("Color maps", IcyColorMap[].class,
            new IcyColorMap[0]);

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("input", sequenceIn);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("Colormaps", colormaps);
    }

    @Override
    public void run()
    {
        final Sequence seq = sequenceIn.getValue();
        if (seq == null)
            throw new VarException(sequenceIn, "Sequence is null");

        final List<IcyColorMap> cms = new ArrayList<IcyColorMap>();

        for (int c = 0; c < seq.getSizeC(); c++)
            cms.add(seq.getColorMap(c));

        colormaps.setValue(cms.toArray(new IcyColorMap[cms.size()]));
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
