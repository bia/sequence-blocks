package plugins.tprovoost.sequenceblocks.files;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.sequence.Sequence;
import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarBoolean;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * Block to save Sequence metadata
 * 
 * @author Stephane
 */
public class SaveMetadata extends Plugin implements SequenceBlock, PluginLibrary, PluginBundled
{
    final protected VarSequence sequence = new VarSequence("Sequence", null);
    final protected VarBoolean success = new VarBoolean("Success", Boolean.FALSE);

    @Override
    public void run()
    {
        Sequence s = sequence.getValue();
        if (s == null)
            throw new VarException(sequence, "Sequence is null");

        success.setValue(Boolean.valueOf(s.saveXMLData()));
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("sequence", sequence);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("success", success);
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
