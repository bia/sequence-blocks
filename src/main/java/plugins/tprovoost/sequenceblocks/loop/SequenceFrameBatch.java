package plugins.tprovoost.sequenceblocks.loop;

import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.sequence.Sequence;
import icy.sequence.SequenceUtil;
import plugins.adufour.blocks.lang.Batch;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * Simple batch loop to iterate over all T frame from an input Sequence.
 * 
 * @author Stephane
 */
public class SequenceFrameBatch extends Batch implements PluginLibrary, PluginBundled
{
    // important to not initialize and create them in getBatchSource() and getBatchElement()
    protected VarSequence inputSequence;
    protected VarSequence element;

    public SequenceFrameBatch()
    {
        super();
    }

    @Override
    public VarSequence getBatchSource()
    {
        // initialize variable if needed
        if (inputSequence == null)
            inputSequence = new VarSequence("Sequence", null);

        return inputSequence;
    }

    @Override
    public VarSequence getBatchElement()
    {
        // initialize element if needed
        if (element == null)
            element = new VarSequence("Frame Sequence", null);

        return element;
    }

    @Override
    public void initializeLoop()
    {
        final Sequence value = inputSequence.getValue();

        if (value == null)
            throw new VarException(inputSequence, "No input sequence indicated");
    }

    @Override
    public void beforeIteration()
    {
        // set result in element
        element.setValue(
                SequenceUtil.extractFrame(inputSequence.getValue(), getIterationCounter().getValue().intValue()));
    }

    @Override
    public boolean isStopConditionReached()
    {
        return getIterationCounter().getValue().intValue() >= inputSequence.getValue().getSizeT();
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
