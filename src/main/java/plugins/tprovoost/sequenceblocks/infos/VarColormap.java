package plugins.tprovoost.sequenceblocks.infos;

import javax.swing.JComboBox;

import org.w3c.dom.Node;

import icy.gui.component.renderer.ColormapComboBoxRenderer;
import icy.image.colormap.IcyColorMap;
import icy.image.colormap.LinearColorMap;
import icy.util.XMLUtil;
import plugins.adufour.vars.gui.VarEditor;
import plugins.adufour.vars.gui.model.ValueSelectionModel;
import plugins.adufour.vars.gui.swing.ComboBox;
import plugins.adufour.vars.lang.Var;

/**
 * Define a specific Var type for ColorMap object
 * 
 * @author emilie
 */
public class VarColormap extends Var<IcyColorMap>
{
    public static class ColormapSelectionModel extends ValueSelectionModel<IcyColorMap>
    {
        public ColormapSelectionModel()
        {
            super(IcyColorMap.getAllColorMaps(true, true).toArray(new IcyColorMap[0]), LinearColorMap.gray_, false);
        }

        @Override
        public boolean isValid(IcyColorMap value)
        {
            return true;
        }
    }

    public final String ID_NODE = "colormap";

    /**
     * Create a new VarColormap with given name and default value
     * @param name String
     * @param defaultValue IcyColorMap
     */
    public VarColormap(String name, IcyColorMap defaultValue)
    {
        super(name, new ColormapSelectionModel());
    }

    @Override
    public VarEditor<IcyColorMap> createVarEditor()
    {
        // create the var editor (combo box type here)
        final ComboBox<IcyColorMap> result = new ComboBox<IcyColorMap>(this);
        // get the editor component
        final JComboBox combo = result.getEditorComponent();
        // and set a specific renderer
        combo.setRenderer(new ColormapComboBoxRenderer(combo));

        return result;
    }

    @Override
    public boolean loadFromXML(Node node)
    {
        try
        {
            final IcyColorMap map = new IcyColorMap();
            map.loadFromXML(XMLUtil.setElement(node, ID_NODE));
            setValue(map);

            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }

    @Override
    public boolean saveToXML(Node node) throws UnsupportedOperationException
    {
        try
        {
            getValue().saveToXML(XMLUtil.setElement(node, ID_NODE));
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }
}
