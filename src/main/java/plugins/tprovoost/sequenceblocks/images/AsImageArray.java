package plugins.tprovoost.sequenceblocks.images;

import icy.image.IcyBufferedImage;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.sequence.Sequence;
import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarArray;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * Returns all images from the Sequence as an image array.
 * 
 * @author thomasprovoost
 */
public class AsImageArray extends Plugin implements SequenceBlock, PluginLibrary, PluginBundled
{
    final protected VarArray<IcyBufferedImage> out = new VarArray<IcyBufferedImage>("Image array", IcyBufferedImage[].class, null);
    final protected VarSequence varSeq = new VarSequence("Sequence", null);

    @Override
    public void run()
    {
        Sequence s = varSeq.getValue();
        if (s == null)
            throw new VarException(varSeq, "Input sequence is null.");
        out.setValue(s.getAllImage().toArray(new IcyBufferedImage[0]));
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("sequence", varSeq);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("image array", out);
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
