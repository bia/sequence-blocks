package plugins.tprovoost.sequenceblocks.files;

import java.io.File;

import icy.file.Loader;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarInteger;
import plugins.adufour.vars.lang.VarMutable;
import plugins.adufour.vars.lang.VarSequence;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * Block to load a Sequence from a file
 * 
 * @author Stephane
 */
public class LoadSequence extends Plugin implements SequenceBlock, PluginLibrary, PluginBundled
{
    final protected VarMutable f_in = new VarMutable("File", null)
    {
        @Override
        public boolean isAssignableFrom(@SuppressWarnings("rawtypes") Var source)
        {
            return (String.class == source.getType()) || (File.class == source.getType());
        }
    };
    final protected VarInteger serie_in = new VarInteger("Series", 0);
    final protected VarSequence outputSequence = new VarSequence("Sequence", null);

    @Override
    public void run()
    {
        final Object obj = f_in.getValue();
        if (obj != null)
        {
            final File f;

            if (obj instanceof String)
                f = new File((String) obj);
            else
                f = (File) obj;

            // if (f.isDirectory())
            // throw new VarException("file should not be a directory.");

            outputSequence.setValue(Loader.loadSequence(f.getAbsolutePath(), serie_in.getValue().intValue(), false));
        }
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("file", f_in);
        inputMap.add("serie", serie_in);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("sequence", outputSequence);

    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
