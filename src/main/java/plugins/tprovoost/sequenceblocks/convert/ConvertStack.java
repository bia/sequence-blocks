package plugins.tprovoost.sequenceblocks.convert;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.sequence.Sequence;
import icy.sequence.SequenceUtil;
import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.BlocksException;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarEnum;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * @author thomasprovoost
 */
public class ConvertStack extends Plugin implements SequenceBlock, PluginBundled
{
    public static enum TypeConversion
    {
        STACK, TIME
    };

    final protected VarEnum<TypeConversion> type = new VarEnum<TypeConversion>("Type Wanted", TypeConversion.STACK);
    final protected VarSequence inputSequence = new VarSequence("Sequence", null);
    final protected VarSequence outputSequence = new VarSequence("Out", null);

    @Override
    public void run()
    {
        Sequence in = inputSequence.getValue();
        if (in == null)
            throw new VarException(inputSequence, "Input sequence is null.");

        try
        {
            // create a copy as we don't want to modify input
            Sequence out = SequenceUtil.getCopy(in);
            if (type.getValue() == TypeConversion.TIME)
                SequenceUtil.convertToTime(out);
            else
                SequenceUtil.convertToStack(out);
            outputSequence.setValue(out);
        }
        catch (InterruptedException e)
        {
            // nothing to do
        }
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("sequence", inputSequence);
        inputMap.add("Type Wanted", type);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("out", outputSequence);
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }

}
