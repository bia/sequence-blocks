package plugins.tprovoost.sequenceblocks.importer;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import plugins.adufour.blocks.tools.io.IOBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * Block to close currently opened SequenceFileImporter.
 * 
 * @author Stephane
 */
public class SequenceFileImporterClose extends Plugin implements IOBlock, PluginLibrary, PluginBundled
{
    final protected Var<PositionedSequenceFileImporter> importer;

    public SequenceFileImporterClose()
    {
        super();

        importer = new Var<PositionedSequenceFileImporter>("Importer", PositionedSequenceFileImporter.class);
    }

    @Override
    public void run()
    {
        final PositionedSequenceFileImporter pi = importer.getValue();

        if (pi == null)
            throw new VarException(importer, "Importer is null !");

        try
        {
            pi.importer.close();
        }
        catch (Exception e)
        {
            throw new VarException(importer, e.getMessage());
        }
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("importer", importer);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        //
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
