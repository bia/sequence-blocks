package plugins.tprovoost.sequenceblocks.infos;

import icy.math.UnitUtil;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.sequence.Sequence;
import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarDouble;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * Block to return pixel size and time interval information from a Sequence object.
 *
 * @see ReadMetadata
 * @see GetMetaData
 * @author Stephane
 */
public class GetResolution extends Plugin implements SequenceBlock, PluginBundled
{
    final protected VarSequence varSequence = new VarSequence("Sequence", null);
    final protected VarDouble pxSizeX = new VarDouble("Pixel Size X (" + UnitUtil.MICRO_STRING + "m)", 1d);
    final protected VarDouble pxSizeY = new VarDouble("Pixel Size Y (" + UnitUtil.MICRO_STRING + "m)", 1d);
    final protected VarDouble pxSizeZ = new VarDouble("Pixel Size Z (" + UnitUtil.MICRO_STRING + "m)", 1d);
    final protected VarDouble timeIntT = new VarDouble("Time interval T (s)", 0.1d);

    @Override
    public void run()
    {
        Sequence s = varSequence.getValue();
        if (s != null)
        {
            pxSizeX.setValue(Double.valueOf(s.getPixelSizeX()));
            pxSizeY.setValue(Double.valueOf(s.getPixelSizeY()));
            pxSizeZ.setValue(Double.valueOf(s.getPixelSizeZ()));
            timeIntT.setValue(Double.valueOf(s.getTimeInterval()));
        }
        else
        {
            throw new VarException(varSequence, "Sequence is null");
        }
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("sequence", varSequence);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("Pixel Size X (" + UnitUtil.MICRO_STRING + "m)", pxSizeX);
        outputMap.add("Pixel Size Y (" + UnitUtil.MICRO_STRING + "m)", pxSizeY);
        outputMap.add("Pixel Size Z (" + UnitUtil.MICRO_STRING + "m)", pxSizeZ);
        outputMap.add("Time interval T (s)", timeIntT);
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
