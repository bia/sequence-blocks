package plugins.tprovoost.sequenceblocks.images;

import icy.image.IcyBufferedImage;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.sequence.Sequence;
import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarInteger;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * Get the image at the given [T,Z] position from the Sequence.
 * 
 * @author thomasprovoost
 */
public class GetImage extends Plugin implements SequenceBlock, PluginLibrary, PluginBundled
{
    final protected VarSequence varSeq = new VarSequence("Sequence", null);
    final protected VarInteger imgIdxT = new VarInteger("T", -1);
    final protected VarInteger imgIdxZ = new VarInteger("Z", -1);
    final protected Var<IcyBufferedImage> out = new Var<IcyBufferedImage>("out", IcyBufferedImage.class);

    @Override
    public void run()
    {
        Sequence s = varSeq.getValue();
        if (s == null)
            throw new VarException(varSeq, "Input sequence is null.");
        out.setValue(s.getImage(imgIdxT.getValue().intValue(), imgIdxZ.getValue().intValue()));
    }

    @Override
    public void declareInput(final VarList inputMap)
    {
        inputMap.add("sequence", varSeq);
        inputMap.add("Idx T", imgIdxT);
        inputMap.add("Idx Z", imgIdxZ);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("out", out);
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
