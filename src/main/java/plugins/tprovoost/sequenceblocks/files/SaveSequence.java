package plugins.tprovoost.sequenceblocks.files;

import java.io.File;

import icy.file.FileUtil;
import icy.file.ImageFileFormat;
import icy.file.Saver;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.sequence.Sequence;
import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarBoolean;
import plugins.adufour.vars.lang.VarEnum;
import plugins.adufour.vars.lang.VarMutable;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * Block to save a Sequence into a file
 * 
 * @author Stephane
 */
public class SaveSequence extends Plugin implements SequenceBlock, PluginLibrary, PluginBundled
{
    final protected VarMutable f_in = new VarMutable("File", null)
    {
        @Override
        public boolean isAssignableFrom(@SuppressWarnings("rawtypes") Var source)
        {
            return String.class == source.getType() || File.class == source.getType();
        }
    };
    final protected VarBoolean overwriteForce = new VarBoolean("Overwrite", Boolean.TRUE);
    final protected VarBoolean multipleFile = new VarBoolean("Multiple file", Boolean.FALSE);
    final protected VarSequence sequence = new VarSequence("Sequence", null);
    final protected VarEnum<ImageFileFormat> format = new VarEnum<ImageFileFormat>("Format", ImageFileFormat.TIFF);

    @Override
    public void run()
    {
        final Sequence seq = sequence.getValue();

        if (seq != null)
        {
            final Object obj = f_in.getValue();

            if (obj != null)
            {
                String path;

                if (obj instanceof String)
                    path = (String) obj;
                else
                    path = ((File) obj).getAbsolutePath();

                if (FileUtil.isDirectory(path))
                    path += FileUtil.separator + FileUtil.getFileName(seq.getOutputFilename(false));

                // force extension depending file format
                path = FileUtil.setExtension(path, "." + format.getValue().getExtensions()[0]);

                // cannot overwrite ??
                if (FileUtil.exists(path) && !overwriteForce.getValue().booleanValue())
                    throw new VarException(f_in,
                            "File already exists. If you want to overwrite, please check the \"overwrite\" field.");

                // save sequence
                Saver.save(seq, new File(path), multipleFile.getValue().booleanValue(), false);
            }
        }
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("file", f_in);
        inputMap.add("sequence", sequence);
        inputMap.add("format", format);
        inputMap.add("overwrite", overwriteForce);
        inputMap.add("multiple file", multipleFile);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        //
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
