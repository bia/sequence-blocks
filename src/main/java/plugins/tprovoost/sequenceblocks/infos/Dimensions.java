package plugins.tprovoost.sequenceblocks.infos;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.sequence.Sequence;
import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarInteger;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * @author thomasprovoost
 */
public class Dimensions extends Plugin implements SequenceBlock, PluginLibrary, PluginBundled
{
    VarSequence varSeq = new VarSequence("Sequence", null);
    VarInteger sizeZ = new VarInteger("Size Z", 1);
    VarInteger sizeT = new VarInteger("Size T", 1);
    VarInteger sizeC = new VarInteger("Size C", 1);
    VarInteger width = new VarInteger("Width", 1);
    VarInteger height = new VarInteger("Height", 1);

    @Override
    public void run()
    {
        Sequence s = varSeq.getValue();
        if (s == null)
            throw new VarException(varSeq, "Input sequence is null.");
        
        width.setValue(Integer.valueOf(s.getWidth()));
        height.setValue(Integer.valueOf(s.getHeight()));
        sizeC.setValue(Integer.valueOf(s.getSizeC()));
        sizeZ.setValue(Integer.valueOf(s.getSizeZ()));
        sizeT.setValue(Integer.valueOf(s.getSizeT()));
    }

    @Override
    public void declareInput(final VarList inputMap)
    {
        inputMap.add("sequence", varSeq);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("width", width);
        outputMap.add("height", height);
        outputMap.add("size C", sizeC);
        outputMap.add("size Z", sizeZ);
        outputMap.add("size T", sizeT);
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
