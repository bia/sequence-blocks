package plugins.tprovoost.sequenceblocks.infos;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.sequence.Sequence;
import icy.util.StringUtil;
import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarInteger;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.lang.VarString;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * Block to set channel name of the specified sequence
 * 
 * @author Stephane
 * @see GetChannelsName
 * @see SetChannelName
 */
public class SetChannelName extends Plugin implements SequenceBlock, PluginBundled
{
    final protected VarSequence varSequence = new VarSequence("Sequence", null);
    final protected VarString varName = new VarString("Name", "");
    final protected VarInteger idx = new VarInteger("Channel", 0);

    @Override
    public void run()
    {
        Sequence s = varSequence.getValue();
        if (s == null)
            throw new VarException(varSequence, "Sequence is null");

        int index = idx.getValue().intValue();
        if (index < 0 || index >= s.getSizeC())
            throw new VarException(idx, "Wrong channel index.");

        String name = varName.getName();
        if (StringUtil.isEmpty(name))
            throw new VarException(varName, "Name cannot be empty");

        s.setChannelName(index, varName.getValue());
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("sequence", varSequence);
        inputMap.add("index", idx);
        inputMap.add("name", varName);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        //
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
