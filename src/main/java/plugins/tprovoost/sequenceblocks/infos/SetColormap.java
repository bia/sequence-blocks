package plugins.tprovoost.sequenceblocks.infos;

import icy.image.colormap.IcyColorMap;
import icy.image.colormap.LinearColorMap;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.sequence.Sequence;
import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.BlocksException;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarInteger;
import plugins.adufour.vars.lang.VarSequence;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

public class SetColormap extends Plugin implements SequenceBlock, PluginBundled
{
    final protected VarSequence sequenceIn = new VarSequence("Sequence", null);
    final protected VarInteger numChannel = new VarInteger("Channel", 0);
    final protected VarColormap colormap = new VarColormap("Color map", LinearColorMap.gray_);

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("input", sequenceIn);
        inputMap.add("numChannel", numChannel);
        inputMap.add("colormap", colormap);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        // no output here
    }

    @Override
    public void run()
    {
        final Sequence seq = sequenceIn.getValue();

        if (seq != null)
        {
            final int ch = numChannel.getValue().intValue();
            final IcyColorMap map = colormap.getValue();

            if ((ch < seq.getSizeC()) && (map != null))
                seq.setColormap(ch, map, map.isAlpha());
            else
                throw new BlocksException("Block 'Set Colormap': illegal channel number for this sequence !", true);
        }
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
