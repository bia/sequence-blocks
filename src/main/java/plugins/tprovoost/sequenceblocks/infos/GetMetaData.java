package plugins.tprovoost.sequenceblocks.infos;

import java.awt.Rectangle;

import icy.math.UnitUtil;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.sequence.Sequence;
import ome.xml.meta.OMEXMLMetadata;
import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarDouble;
import plugins.adufour.vars.lang.VarInteger;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.lang.VarString;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * Block to return metadata information from a Sequence object.
 *
 * @see ReadMetadata
 * @author Stephane
 */
public class GetMetaData extends Plugin implements SequenceBlock, PluginBundled
{
    final protected VarSequence varSequence = new VarSequence("Sequence", null);
    final protected Var<OMEXMLMetadata> metaData = new Var<OMEXMLMetadata>("Metadata", OMEXMLMetadata.class);
    final protected VarString name = new VarString("Name", "");
    final protected VarInteger seriesIndex = new VarInteger("Series index", 0);
    final protected VarDouble pxSizeX = new VarDouble("Pixel Size X (" + UnitUtil.MICRO_STRING + "m)", 1d);
    final protected VarDouble pxSizeY = new VarDouble("Pixel Size Y (" + UnitUtil.MICRO_STRING + "m)", 1d);
    final protected VarDouble pxSizeZ = new VarDouble("Pixel Size Z (" + UnitUtil.MICRO_STRING + "m)", 1d);
    final protected VarDouble timeIntT = new VarDouble("Time interval T (s)", 100d);
    final protected VarDouble positionX = new VarDouble("Position X (" + UnitUtil.MICRO_STRING + "m)", 0d);
    final protected VarDouble positionY = new VarDouble("Position Y (" + UnitUtil.MICRO_STRING + "m)", 0d);
    final protected VarDouble positionZ = new VarDouble("Position Z (" + UnitUtil.MICRO_STRING + "m)", 0d);
    final protected VarInteger originResolution = new VarInteger("Resolution", 0);
    final protected Var<Rectangle> originRegion = new Var<Rectangle>("Region", new Rectangle());
    final protected VarInteger originZMin = new VarInteger("Z min", -1);
    final protected VarInteger originZMax = new VarInteger("Z max", -1);
    final protected VarInteger originTMin = new VarInteger("T min", -1);
    final protected VarInteger originTMax = new VarInteger("T max", -1);
    final protected VarInteger originChannel = new VarInteger("Channel", -1);

    @Override
    public void run()
    {
        Sequence s = varSequence.getValue();

        if (s == null)
            throw new VarException(varSequence, "Sequence is null");

        metaData.setValue(s.getOMEXMLMetadata());
        name.setValue(s.getName());
        seriesIndex.setValue(Integer.valueOf(s.getSeries()));
        pxSizeX.setValue(Double.valueOf(s.getPixelSizeX()));
        pxSizeY.setValue(Double.valueOf(s.getPixelSizeY()));
        pxSizeZ.setValue(Double.valueOf(s.getPixelSizeZ()));
        timeIntT.setValue(Double.valueOf(s.getTimeInterval()));
        positionX.setValue(Double.valueOf(s.getPositionX()));
        positionY.setValue(Double.valueOf(s.getPositionY()));
        positionZ.setValue(Double.valueOf(s.getPositionZ()));
        originResolution.setValue(Integer.valueOf(s.getOriginResolution()));
        originRegion.setValue(s.getOriginXYRegion());
        originZMin.setValue(Integer.valueOf(s.getOriginZMin()));
        originZMax.setValue(Integer.valueOf(s.getOriginZMax()));
        originTMin.setValue(Integer.valueOf(s.getOriginTMin()));
        originTMax.setValue(Integer.valueOf(s.getOriginTMax()));
        originChannel.setValue(Integer.valueOf(s.getOriginChannel()));
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("sequence", varSequence);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("metadata", metaData);
        outputMap.add("name", name);
        outputMap.add("seriesIndex", seriesIndex);
        outputMap.add("Pixel Size X (mm)", pxSizeX);
        outputMap.add("Pixel Size Y (mm)", pxSizeY);
        outputMap.add("Pixel Size Z (mm)", pxSizeZ);
        outputMap.add("Time interval T (ms)", timeIntT);
        outputMap.add("positionx", positionX);
        outputMap.add("positiony", positionY);
        outputMap.add("positionz", positionZ);
        outputMap.add("originResolution", originResolution);
        outputMap.add("originRegion", originRegion);
        outputMap.add("originZMin", originZMin);
        outputMap.add("originZMax", originZMax);
        outputMap.add("originTMin", originTMin);
        outputMap.add("originTMax", originTMax);
        outputMap.add("originChannel", originChannel);
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
