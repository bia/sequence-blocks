package plugins.tprovoost.sequenceblocks.infos;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.sequence.Sequence;
import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarArray;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * Block to set all channels name of the specified sequence
 * 
 * @author Stephane
 * @see GetChannelsName
 * @see SetChannelName
 */
public class SetChannelsName extends Plugin implements SequenceBlock, PluginBundled
{
    final protected VarSequence varSequence = new VarSequence("Sequence", null);
    final protected VarArray<String> varNames = new VarArray<String>("Names", String[].class, new String[0]);

    @Override
    public void run()
    {
        final Sequence s = varSequence.getValue();
        if (s == null)
            throw new VarException(varSequence, "Sequence is null");

        final String[] names = varNames.getValue();
        if (names == null)
            throw new VarException(varNames, "Names is null");

        for (int c = 0; c < Math.min(s.getSizeC(), names.length); c++)
            s.setChannelName(c, names[c]);
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("sequence", varSequence);
        inputMap.add("names", varNames);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        //
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
