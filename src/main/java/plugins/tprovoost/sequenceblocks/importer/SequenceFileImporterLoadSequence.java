package plugins.tprovoost.sequenceblocks.importer;

import java.awt.Rectangle;

import icy.file.Loader;
import icy.file.SequenceFileImporter;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginLibrary;
import icy.roi.ROI;
import ome.xml.meta.OMEXMLMetadata;
import plugins.adufour.blocks.tools.io.IOBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarInteger;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.util.VarException;
import plugins.tprovoost.sequenceblocks.SequenceBlocks;

/**
 * Block to returns a Sequence from the given parameters and opened SequenceFileImporter.
 * 
 * @author Stephane
 */
public class SequenceFileImporterLoadSequence extends Plugin implements IOBlock, PluginLibrary, PluginBundled
{
    final protected Var<PositionedSequenceFileImporter> importer;
    final protected VarInteger series;
    final protected VarInteger resolution;
    final protected VarROIArray region;
    final protected VarInteger minZIndex;
    final protected VarInteger maxZIndex;
    final protected VarInteger minTIndex;
    final protected VarInteger maxTIndex;
    final protected VarInteger cIndex;

    final protected VarSequence sequence;

    public SequenceFileImporterLoadSequence()
    {
        super();

        importer = new Var<PositionedSequenceFileImporter>("Importer", PositionedSequenceFileImporter.class);

        series = new VarInteger("Series", -1);
        resolution = new VarInteger("Resolution (0=full, 1=1/2, ..)", 0);
        region = new VarROIArray("XY region (ROI)", null);
        minZIndex = new VarInteger("Z min (slice)", -1);
        maxZIndex = new VarInteger("Z max (slice)", -1);
        minTIndex = new VarInteger("T min (frame)", -1);
        maxTIndex = new VarInteger("T max (frame)", -1);
        cIndex = new VarInteger("C (channel) index", -1);

        sequence = new VarSequence("Sequence", null);
    }

    @Override
    public void run()
    {
        final PositionedSequenceFileImporter pi = importer.getValue();

        if (pi == null)
            throw new VarException(importer, "Importer is null !");

        final SequenceFileImporter imp = pi.importer;

        if (imp.getOpened() == null)
            throw new VarException(importer, "Importer is not opened !");

        final int res = resolution.getValue().intValue();
        final ROI[] rois = region.getValue();

        int s = series.getValue().intValue();
        int minZ = minZIndex.getValue().intValue();
        int maxZ = maxZIndex.getValue().intValue();
        int minT = minTIndex.getValue().intValue();
        int maxT = maxTIndex.getValue().intValue();
        int c = cIndex.getValue().intValue();
        Rectangle rect = ((rois != null) && (rois.length > 0)) ? rois[0].getBounds5D().toRectangle2D().getBounds()
                : null;

        // undefined values ? use internal position if any defined
        if ((s == -1) && (pi.s != -1))
            s = pi.s;
        if ((minZ == -1) && (maxZ == -1) && (pi.z != -1))
        {
            minZ = pi.z;
            maxZ = pi.z;
        }
        if ((minT == -1) && (maxT == -1) && (pi.t != -1))
        {
            minT = pi.t;
            maxT = pi.t;
        }
        if ((c == -1) && (pi.c != -1))
            c = pi.c;
        if ((rect == null) && (pi.xyRegion != null))
            rect = pi.xyRegion;
        
        // still undefined ? --> set default value for series
        if (s == -1)
            s = 0;

        try
        {
            
            // get metadata
            final OMEXMLMetadata meta = pi.getMetadata();

            sequence.setValue(Loader.internalLoadSingle(imp, meta, s, res, rect, minZ, maxZ, minT, maxT, c, false, null));
        }
        catch (Exception e)
        {
            throw new VarException(importer, e.getMessage());
        }
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("importer", importer);
        inputMap.add("series", series);
        inputMap.add("resolution", resolution);
        inputMap.add("region", region);
        inputMap.add("minZ", minZIndex);
        inputMap.add("maxZ", maxZIndex);
        inputMap.add("minT", minTIndex);
        inputMap.add("maxT", maxTIndex);
        inputMap.add("cIndex", cIndex);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("sequence", sequence);
    }

    @Override
    public String getMainPluginClassName()
    {
        return SequenceBlocks.class.getName();
    }
}
